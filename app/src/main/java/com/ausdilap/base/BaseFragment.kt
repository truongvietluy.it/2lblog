package com.ausdilap.base

import android.Manifest
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import com.ausdilap.R
import com.ausdilap.utils.showToast
import com.ausdilap.widget.ConfirmDialog
import com.ausdilap.widget.LoadingDialog
import com.tbruyelle.rxpermissions3.RxPermissions
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo

abstract class BaseFragment<T: BaseViewModel> : Fragment() {
    protected abstract val viewModel: T
    protected val compositeDisposable = CompositeDisposable()
    protected val rxPermissions by lazy { RxPermissions(this) }

    @LayoutRes
    abstract fun getLayoutId(): Int

    private val dialogLoading by lazy { LoadingDialog(requireContext()) }

    open fun initialize() {}
    open fun setEventListener() {}
    open fun observeViewModel() {}

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return layoutInflater.inflate(getLayoutId(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initialize()
        setEventListener()
        observeViewModel()
    }

    private fun showToastEvent(@StringRes message: Int) {
        if (message != -1) {
            requireContext().showToast(message)
        }
    }

    private fun showToastEvent(message: String) {
        requireContext().showToast(message)
    }

    fun showLoading(canCancel: Boolean = false) {
        dialogLoading.setCancelable(canCancel)
        if (!dialogLoading.isShowing) {
            dialogLoading.show()
        }
    }

    fun hideLoading() {
        if (dialogLoading.isShowing) {
            dialogLoading.dismiss()
        }
    }

    fun requestPermissionForTakeImage(onSuccess: () -> Unit) {
        askPermissionsThen(
            listOf(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
            )
        ) {
            onSuccess.invoke()
        }
    }

    private fun askPermissionsThen(
        permission: List<String>,
        onSuccess: () -> Unit
    ) {
        rxPermissions
            .request(*permission.toTypedArray())
            .subscribe { result ->
                if (result) {
                    onSuccess.invoke()
                } else {
                    showPermissionPopup()
                }
            }
            .addTo(compositeDisposable)
    }

    private fun showPermissionPopup() {
        ConfirmDialog(
            context = requireContext(),
            title = getString(R.string.rermission_denied),
            content = getString(R.string.msg_permission_setting),
            submitText = getString(R.string.msg_permission_setting_positive),
            cancelText = getString(R.string.msg_permission_setting_negative),
            onSubmitCallback = {
                val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                val uri = Uri.fromParts("package", requireActivity().packageName, null)
                intent.data = uri
                startActivity(intent)
            },
            onCancelCallback = {}
        ).show()
    }

}