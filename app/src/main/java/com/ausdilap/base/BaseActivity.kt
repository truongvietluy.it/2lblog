package com.ausdilap.base

import android.Manifest
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.ausdilap.R
import com.ausdilap.utils.showToast
import com.ausdilap.widget.ConfirmDialog
import com.ausdilap.widget.LoadingDialog
import com.tbruyelle.rxpermissions3.RxPermissions
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo

abstract class BaseActivity<T : BaseViewModel> : AppCompatActivity() {

    private val dialogLoading by lazy { LoadingDialog(this) }

    protected abstract val viewModel: T

    protected val compositeDisposable = CompositeDisposable()
    protected val rxPermissions by lazy { RxPermissions(this) }

    @LayoutRes
    abstract fun getLayoutId(): Int

    open fun initialize() {}
    open fun setEventListener() {}
    open fun observeViewModel() {}

    override fun onCreate(savedInstanceState: Bundle?) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())

        initialize()
        setEventListener()
        observeViewModel()
    }

    private fun showToastEvent(@StringRes message: Int) {
        if (message != -1) {
            showToast(message)
        }
    }

    private fun showToastEvent(message: String) {
        showToast(message)
    }

    fun replaceFragment(
        containerId: Int,
        fragment: Fragment,
        tag: String? = null,
        addToBackStack: Boolean = false
    ) {
        val transaction = supportFragmentManager
            .beginTransaction()
        transaction
            .replace(containerId, fragment, tag)
        if (addToBackStack) {
            transaction.addToBackStack(this.localClassName)
        }
        transaction.commit()
    }

    fun setStatusBarColor(colorId: Int) {
        window?.statusBarColor = ContextCompat.getColor(this, colorId)
    }

    fun replaceFragment(
        containerId: Int,
        fragment: Class<out Fragment>,
        arg: Bundle? = null,
        tag: String? = null,
        addToBackStack: Boolean = false
    ) {
        val transaction = supportFragmentManager
            .beginTransaction()
        transaction
            .replace(containerId, fragment, arg, tag)
        if (addToBackStack) {
            transaction.addToBackStack(this.localClassName)
        }
        transaction.commit()
    }

    fun showLoading(canCancel: Boolean = false) {
        dialogLoading.setCancelable(canCancel)
        if (!dialogLoading.isShowing) {
            dialogLoading.show()
        }
    }

    fun hideLoading() {
        if (dialogLoading.isShowing) {
            dialogLoading.hide()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        dialogLoading.dismiss()
    }

    fun requestPermissionForTakeImage(onSuccess: () -> Unit) {
        askPermissionsThen(
            listOf(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
            )
        ) {
            onSuccess.invoke()
        }
    }

    private fun askPermissionsThen(
        permission: List<String>,
        onSuccess: () -> Unit
    ) {
        rxPermissions
            .request(*permission.toTypedArray())
            .subscribe { result ->
                if (result) {
                    onSuccess.invoke()
                } else {
                    showPermissionPopup()
                }
            }
            .addTo(compositeDisposable)
    }

    private fun showPermissionPopup() {
        ConfirmDialog(
            context = this,
            title = getString(R.string.rermission_denied),
            content = getString(R.string.msg_permission_setting),
            submitText = getString(R.string.msg_permission_setting_positive),
            cancelText = getString(R.string.msg_permission_setting_negative),
            onSubmitCallback = {
                val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                val uri = Uri.fromParts("package", packageName, null)
                intent.data = uri
                startActivity(intent)
            },
            onCancelCallback = {}
        ).show()
    }
}