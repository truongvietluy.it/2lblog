package com.ausdilap.widget

import com.ausdilap.R
import com.ausdilap.ext.hide
import com.ausdilap.ext.setVisible
import kotlinx.android.synthetic.main.bottom_sheet_select.*

open class SelectBottomSheetFragment(
    private val title: String,
    private val textOption1: String? = null,
    private val colorOption1: Int? = null,
    private val textOption2: String? = null,
    private val colorOption2: Int? = null,
    private val textOption3: String? = null,
    private val colorOption3: Int? = null,
    private val textOption4: String? = null,
    private val colorOption4: Int? = null,
    private val textOption5: String? = null,
    private val colorOption5: Int? = null,
    private val textOption6: String? = null,
    private val colorOption6: Int? = null,
    private val onOption1Callback: (() -> Unit?)? = null,
    private val onOption2Callback: (() -> Unit?)? = null,
    private val onOption3Callback: (() -> Unit?)? = null,
    private val onOption4Callback: (() -> Unit?)? = null,
    private val onOption5Callback: (() -> Unit?)? = null,
    private val onOption6Callback: (() -> Unit?)? = null,
    private val onCancelCallback: () -> Unit?
) : TransparentBottomSheetFragment() {
    override fun setLayoutId(): Int {
        return R.layout.bottom_sheet_select
    }

    override fun init() {
        tvTitle.text = title

        tvOption1.setVisible(textOption1 != null)
        if (textOption1 != null) {
            tvOption1.text = textOption1
            tvOption1.setOnClickListener {
                onOption1Callback?.invoke()
                this.dismiss()
            }
        }

        tvOption2.setVisible(textOption2 != null)
        viewOption2.setVisible(textOption2 != null)
        if (textOption2 != null) {
            tvOption2.text = textOption2
            tvOption2.setOnClickListener {
                onOption2Callback?.invoke()
                this.dismiss()
            }
        }

        tvOption3.setVisible(textOption3 != null)
        viewOption3.setVisible(textOption3 != null)
        if (textOption3 != null) {
            tvOption3.text = textOption3
            tvOption3.setOnClickListener {
                onOption3Callback?.invoke()
                this.dismiss()
            }
        }

        tvOption4.setVisible(textOption4 != null)
        viewOption4.setVisible(textOption4 != null)
        if (textOption4 != null) {
            tvOption4.text = textOption4
            tvOption4.setOnClickListener {
                onOption4Callback?.invoke()
                this.dismiss()
            }
        }

        tvOption5.setVisible(textOption5 != null)
        viewOption5.setVisible(textOption5 != null)
        if (textOption5 != null) {
            tvOption5.text = textOption5
            tvOption5.setOnClickListener {
                onOption5Callback?.invoke()
                this.dismiss()
            }
        }

        tvOption6.setVisible(textOption6 != null)
        viewOption6.setVisible(textOption6 != null)
        if (textOption6 != null) {
            tvOption6.text = textOption6
            tvOption6.setOnClickListener {
                onOption6Callback?.invoke()
                this.dismiss()
            }
        }

        ivClose.setOnClickListener {
            onCancelCallback.invoke()
            this.dismiss()
        }
    }
}

fun SelectBottomSheetFragment.hideTitle(): SelectBottomSheetFragment {
    tvTitle.hide()
    return this
}