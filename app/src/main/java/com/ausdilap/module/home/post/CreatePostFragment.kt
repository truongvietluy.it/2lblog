package com.ausdilap.module.home.post

import android.net.Uri
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.ausdilap.R
import com.ausdilap.base.BaseFragment
import com.ausdilap.ext.hide
import com.ausdilap.ext.setOnDelayClickListener
import com.ausdilap.utils.CameraUtils
import com.ausdilap.utils.observe
import com.ausdilap.utils.showToast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_create_post.*

@AndroidEntryPoint
class CreatePostFragment : BaseFragment<CreatePostViewModel>() {

    override val viewModel: CreatePostViewModel by viewModels()
    private lateinit var addImageAdapter: AddImageAdapter

    private var listImages = mutableListOf<String>()
    private var images = mutableListOf<Uri>()

    override fun getLayoutId(): Int {
        return R.layout.fragment_create_post
    }

    override fun initialize() {
        super.initialize()
        initAdapter()
        initRecyclerView()
        initEvent()
        initData()
        initObserve()
    }

    private fun initData() {

    }

    private fun initObserve() {
        observe(viewModel.createPostObs) {
            when(it) {
                is CreatePostViewModel.CreatePostState.Loading -> {
                    showLoading()
                }

                is CreatePostViewModel.CreatePostState.Success -> {
                    hideLoading()
                    activity?.showToast("Success")
                    findNavController().popBackStack()
                }

                is CreatePostViewModel.CreatePostState.Failed -> {
                    hideLoading()
                    findNavController().popBackStack()
                }
            }
        }
    }

    private fun initRecyclerView() {
        rvMedia.apply {
            adapter = addImageAdapter
            layoutManager = GridLayoutManager(context, 2, LinearLayoutManager.VERTICAL, false)
        }
    }

    private fun initAdapter() {
        addImageAdapter = AddImageAdapter(
            onRemoveItemClickListener = {
                addImageAdapter.removeItem(it)
                listImages.remove(it)
                listImages.size

                clAddImage.isEnabled = addImageAdapter.itemCount <= 3
            }
        )
    }

    private fun initEvent() {
        ivClose.setOnDelayClickListener {
            findNavController().popBackStack()
        }

        clAddImage.setOnDelayClickListener {
            requestPermissionForTakeImage {
                CameraUtils.chooseSinglePhoto(requireContext()) { path ->
                    listImages.add(path)
                    addImageAdapter.updateImage(listImages)

                    if (addImageAdapter.itemCount > 3) {
                        clAddImage.isEnabled = false
                    }

                    tvCreate.isEnabled = addImageAdapter.itemCount == 4
                }
            }
        }

        tvCreate.setOnDelayClickListener {

            listImages.forEach {
                images.add(Uri.parse(it))
            }
            viewModel.requestCreatePost(etAddress.text.toString(), etDescription.text.toString(), images)
        }
    }
}