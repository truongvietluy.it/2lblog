package com.ausdilap.module.home.post

import android.net.Uri
import androidx.activity.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.ausdilap.R
import com.ausdilap.base.BaseActivity
import com.ausdilap.ext.setOnDelayClickListener
import com.ausdilap.module.home.home.ImageAdapter
import com.ausdilap.utils.CameraUtils
import com.ausdilap.utils.observe
import com.ausdilap.utils.showToast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_create_post.*

@AndroidEntryPoint
class CreatePostActivity : BaseActivity<CreatePostViewModel>() {

    override val viewModel: CreatePostViewModel by viewModels()

    private lateinit var imageAdapter: ImageAdapter

    private var listImages = mutableListOf<String>()
    private var images = mutableListOf<Uri>()

    override fun getLayoutId(): Int {
        return R.layout.activity_create_post
    }

    override fun initialize() {
        super.initialize()
        initAdapter()
        initRecyclerView()
        initEvent()
        initObserve()
    }

    private fun initObserve() {
        observe(viewModel.createPostObs) {
            when(it) {
                is CreatePostViewModel.CreatePostState.Loading -> {
                    showLoading()
                }

                is CreatePostViewModel.CreatePostState.Success -> {
                    hideLoading()
                    showToast("Success")
                    finish()
                }

                is CreatePostViewModel.CreatePostState.Failed -> {
                    hideLoading()
                    showToast(it.message)
                }
            }
        }
    }

    private fun initRecyclerView() {
        rvMedia.apply {
            adapter = imageAdapter
            layoutManager = GridLayoutManager(context, 2, LinearLayoutManager.VERTICAL, false)
        }
    }

    private fun initAdapter() {
        imageAdapter = ImageAdapter(
            onItemClickListener = {}
        )
    }

    private fun initEvent() {
        ivClose.setOnDelayClickListener {
            finish()
        }

        clAddImage.setOnDelayClickListener {
            requestPermissionForTakeImage {
                CameraUtils.chooseSinglePhoto(this) { path ->
                    val image : Uri = Uri.parse(path)
                    images.add(image)
                    listImages.add(path)
                    imageAdapter.updateImage(listImages)
                }
            }
        }

        tvCreate.setOnDelayClickListener {
            viewModel.requestCreatePost(etAddress.text.toString(), etDescription.text.toString(), images)
        }
    }

}