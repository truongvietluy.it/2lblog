package com.ausdilap.module.home.save

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ausdilap.base.BaseViewModel
import com.ausdilap.data.model.Post
import com.ausdilap.data.model.SaveLikeParam
import com.ausdilap.data.parameter.SavePostParameter
import com.ausdilap.data.usecase.post.DeletePostUseCase
import com.ausdilap.data.usecase.post.GetRememberUseCase
import com.ausdilap.data.usecase.post.RememberPostUseCase
import com.ausdilap.data.usecase.post.SaveLikeUseCase
import com.ausdilap.data.usecase.user.FollowUserUseCase
import com.ausdilap.error.AppError
import com.ausdilap.error.ErrorHandler
import com.ausdilap.error.ErrorUtils
import com.ausdilap.module.auth.model.User
import com.ausdilap.module.home.home.HomeViewModel
import com.ausdilap.pref.UserPref
import com.ausdilap.utils.SingleLiveEvent
import com.ausdilap.utils.addTo
import com.ausdilap.utils.applyBackgroundStream
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SavedViewModel @Inject constructor(
    private val getRememberUseCase: GetRememberUseCase,
    private val deletePostUseCase: DeletePostUseCase,
    private val rememberPostUseCase: RememberPostUseCase,
    private val followUserUseCase: FollowUserUseCase,
    private val userPref: UserPref,
    private val saveLikeUseCase: SaveLikeUseCase,
    private val errorUtils: ErrorUtils
) : BaseViewModel() {

    sealed class GetRememberState {
        object Loading : GetRememberState()
        class Success(val listPost: MutableList<Post>) : GetRememberState()
        class Failed(val message: String) : GetRememberState()
    }

    sealed class DeleteState {
        object Loading : DeleteState()
        class Success(val message: String): DeleteState()
        class Failed(val message: String): DeleteState()
    }

    sealed class SaveLikeState {
        object Loading : SaveLikeState()
        class Success(val saveLikeParam: SaveLikeParam) : SaveLikeState()
        class Failed(val message: String) : SaveLikeState()
    }

    sealed class SavePostSate {
        object Loading : SavePostSate()
        class Success(val savePostParameter: SavePostParameter) : SavePostSate()
        class Failed(val message: String) : SavePostSate()
    }

    sealed class FollowUserState {
        object Loading : FollowUserState()
        class Success(val message: String) : FollowUserState()
        class Failed(val message: String) : FollowUserState()
    }

    sealed class RequestLikeState {
        object Loading : RequestLikeState()
        class Failed(val message: String) : RequestLikeState()
        class Success(val post: Post) : RequestLikeState()
    }

    private val _getRememberObs = MutableLiveData<GetRememberState>()
    val getRememberObs : LiveData<GetRememberState> = _getRememberObs

    private val _deletePostObs = MutableLiveData<DeleteState>()
    val deletePostObs : LiveData<DeleteState> = _deletePostObs

    private val _saveLikeObs = MutableLiveData<SaveLikeState>()
    val saveLikeObs: LiveData<SaveLikeState> = _saveLikeObs

    private val _savePostObs = MutableLiveData<SavePostSate>()
    val savePostObs: LiveData<SavePostSate> = _savePostObs

    private val _followUserObs = MutableLiveData<FollowUserState>()
    val followUserObs: LiveData<FollowUserState> = _followUserObs

    val userProfileObs = SingleLiveEvent<User?>()

    private val _requestLikePostChangeObs = MutableLiveData<RequestLikeState>()
    val requestLikePostChangeObs: LiveData<RequestLikeState> = _requestLikePostChangeObs

    fun getUserProfile() {
        userProfileObs.postValue(userPref.getUserProfile())
    }

    fun saveLike(id: String) {
        _saveLikeObs.postValue(SaveLikeState.Loading)
        saveLikeUseCase
            .execute(id)
            .applyBackgroundStream()
            .subscribe({
                _saveLikeObs.postValue(SaveLikeState.Success(it))
            }, {
                val error = ErrorHandler.getError(it, AppError::class.java, errorUtils)
                _saveLikeObs.postValue(
                    SaveLikeState.Failed(error.provideErrorMessage())
                )
            }).addTo(this)
    }

    fun getRemember() {
        _getRememberObs.postValue(GetRememberState.Loading)
        getRememberUseCase.execute()
            .applyBackgroundStream()
            .subscribe({
                _getRememberObs.postValue(GetRememberState.Success(it))
            }, {
                val error = ErrorHandler.getError(it, AppError::class.java, errorUtils)
                _getRememberObs.postValue(GetRememberState.Failed(error.provideErrorMessage()))
            })
    }

    fun deletePost(id: String) {
        _deletePostObs.postValue(DeleteState.Loading)
        deletePostUseCase.execute(id)
            .applyBackgroundStream()
            .subscribe({
                _deletePostObs.postValue(DeleteState.Success(it))
            }, {
                val error = ErrorHandler.getError(it, AppError::class.java, errorUtils)
                _deletePostObs.postValue(DeleteState.Failed(error.provideErrorMessage()))
            })
    }

    fun savePost(userId: String, postId: String, id: String) {
        _savePostObs.postValue(SavePostSate.Loading)
        rememberPostUseCase.execute(userId, postId, id)
            .applyBackgroundStream()
            .subscribe({
                _savePostObs.postValue(SavePostSate.Success(it))
            }, {
                val error = ErrorHandler.getError(it, AppError::class.java, errorUtils)
                _savePostObs.postValue(SavePostSate.Failed(error.provideErrorMessage()))
            })
    }

    fun followUser(myId: String, userId: String) {
        _followUserObs.postValue(FollowUserState.Loading)
        followUserUseCase.execute(myId, userId)
            .applyBackgroundStream()
            .subscribe({
                _followUserObs.postValue(FollowUserState.Success(it))
            }, {
                val error = ErrorHandler.getError(it, AppError::class.java, errorUtils)
                _followUserObs.postValue(FollowUserState.Failed(error.provideErrorMessage()))
            })
    }

    fun requestLikePost(post: Post) {
        post.isLike = post.isLike?.not()
        _requestLikePostChangeObs.postValue(RequestLikeState.Success(post))
    }
}