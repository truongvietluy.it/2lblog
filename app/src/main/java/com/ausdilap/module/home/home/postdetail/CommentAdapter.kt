package com.ausdilap.module.home.home.postdetail

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ausdilap.R
import com.ausdilap.base.BaseAdapter
import com.ausdilap.data.model.Comment
import com.ausdilap.ext.loadImageUri
import com.ausdilap.ext.setOnDelayClickListener
import kotlinx.android.synthetic.main.item_comment.view.*

class CommentAdapter(
    private val onOpenCommentOptions: (Comment) -> Unit
) : BaseAdapter<CommentAdapter.CommentVH>() {

    private val listComments = mutableListOf<Comment>()
    class CommentVH(
        view: View,
        private val onOpenCommentOptions: (Comment) -> Unit
    ) : RecyclerView.ViewHolder(view) {
        fun bind(comment: Comment) {
            itemView.apply {
                ivUserAvt.loadImageUri(Uri.parse(comment.user.avatar))
                tvUserName.text = comment.user.name
                tvContent.text = comment.contents
                tvDate.text = comment.createdAt

                setOnDelayClickListener {
                    onOpenCommentOptions.invoke(comment)
                }
            }
        }
    }

    fun updateComment(comments: MutableList<Comment>) {
        listComments.clear()
        listComments.addAll(comments)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentVH {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_comment, parent, false)
        return CommentVH(view, onOpenCommentOptions)
    }

    override fun onBindViewHolder(holder: CommentVH, position: Int) {
        holder.bind(listComments[position])
    }

    override fun getItemCount(): Int {
        return listComments.size
    }

}