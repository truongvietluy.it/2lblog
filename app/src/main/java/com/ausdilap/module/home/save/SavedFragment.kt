package com.ausdilap.module.home.save

import android.content.Intent
import android.os.Bundle
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.ausdilap.R
import com.ausdilap.base.BaseFragment
import com.ausdilap.ext.hide
import com.ausdilap.ext.setVisible
import com.ausdilap.ext.show
import com.ausdilap.module.home.home.HomeFragment
import com.ausdilap.module.home.home.PostAdapter
import com.ausdilap.module.home.home.ViewMediaFragment
import com.ausdilap.module.home.home.postoptions.PostOptionBottomSheet
import com.ausdilap.utils.observe
import com.ausdilap.utils.showToast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_home.rvPost
import kotlinx.android.synthetic.main.fragment_home.viewLoading
import kotlinx.android.synthetic.main.fragment_saved.*

@AndroidEntryPoint
class SavedFragment : BaseFragment<SavedViewModel>() {

    override val viewModel: SavedViewModel by viewModels()

    override fun getLayoutId(): Int = R.layout.fragment_saved

    private lateinit var postAdapter: PostAdapter

    private var myId: String? = null
    private var listImages = arrayListOf<String>()

    override fun initialize() {
        super.initialize()
        initData()
        initAdapter()
        initRV()

        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true ) {
                override fun handleOnBackPressed() {
                    findNavController().popBackStack()
                }
            }

        activity?.onBackPressedDispatcher?.addCallback(this, callback)
    }

    private fun initRV() {
        rvPost.apply {
            adapter = postAdapter
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        }
    }

    private fun initAdapter() {
        postAdapter = PostAdapter(
            onItemClickListener = { post ->
                val bundle = Bundle()
                bundle.putString(HomeFragment.POST_ID, post.id)
                findNavController().navigate(R.id.goToPostDetailFragment, bundle)
            },
            onMoreOptionsClickListener = { post ->
                post.isRemember?.let { isRemember ->
                    PostOptionBottomSheet(
                        isRemember = isRemember,
                        isFollow = post.isFollow,
                        onDeletePost = {
                            post.id?.let { postID -> viewModel.deletePost(postID) }
                        },
                        isHideDelete = (myId != post.userId),
                        onSavePost = {
                            post.userId?.let { userId ->
                                post.id?.let { postId ->
                                    viewModel.savePost(
                                        userId,
                                        postId,
                                        postId
                                    )
                                }
                            }
                        },
                        isHideSave = (false),
                        onFollow = {
                            myId?.let { myId ->
                                post.userId?.let { userID ->
                                    viewModel.followUser(myId, userID)
                                }
                            }
                        },
                        isHideFollow = (myId == post.userId)
                    ).show(parentFragmentManager, PostOptionBottomSheet::class.simpleName)
                }
            },
            onLikeClickListener = { post ->
                post.id?.let { postId -> viewModel.saveLike(postId) }
                viewModel.requestLikePost(post)
            },
            onFollowUser = {},
            onViewImageClickListener = { post ->
                post.images?.forEach { image ->
                    image.pathImage?.let { it1 -> listImages.add(it1) }
                }
                val bundle = Bundle()
                bundle.putStringArrayList(ViewMediaFragment.MEDIAS, listImages)
                findNavController().navigate(R.id.goToViewMediaFragment, bundle)
            }
        )
    }

    private fun initData() {
        viewModel.getRemember()
    }

    override fun observeViewModel() {
        super.observeViewModel()

        observe(viewModel.getRememberObs) {
            when (it) {
                is SavedViewModel.GetRememberState.Loading -> {
                    viewLoading.setVisible(postAdapter.itemCount == 0)
                }

                is SavedViewModel.GetRememberState.Success -> {
                    viewLoading.setVisible(false)
                    postAdapter.updatePost(it.listPost)
                    if (postAdapter.itemCount == 0) {
                        llEmptySaved.show()
                    } else {
                        llEmptySaved.hide()
                    }
                }

                is SavedViewModel.GetRememberState.Failed -> {
                    viewLoading.setVisible(false)
                    activity?.showToast(it.message)
                }
            }
        }

        observe(viewModel.deletePostObs) {
            when (it) {
                is SavedViewModel.DeleteState.Loading -> {
                    showLoading()
                }

                is SavedViewModel.DeleteState.Success -> {
                    hideLoading()
                }
                is SavedViewModel.DeleteState.Failed -> {
                    hideLoading()
                    activity?.showToast(it.message)
                }
            }
        }

        observe(viewModel.saveLikeObs) {
            when (it) {
                is SavedViewModel.SaveLikeState.Loading -> {
                }
                is SavedViewModel.SaveLikeState.Success -> {
                    viewModel.getRemember()
                }
                is SavedViewModel.SaveLikeState.Failed -> {
                }
            }
        }

        observe(viewModel.savePostObs) {
            when (it) {
                is SavedViewModel.SavePostSate.Loading -> {
                    showLoading()
                }

                is SavedViewModel.SavePostSate.Success -> {
                    hideLoading()
                }

                is SavedViewModel.SavePostSate.Failed -> {
                    hideLoading()
                }
            }
        }

        observe(viewModel.followUserObs) {
            when (it) {
                is SavedViewModel.FollowUserState.Loading -> {
                    showLoading()
                }

                is SavedViewModel.FollowUserState.Success -> {
                    hideLoading()
                    viewModel.getRemember()
                }

                is SavedViewModel.FollowUserState.Failed -> {
                    hideLoading()
                    viewModel.getRemember()
                }
            }
        }

        observe(viewModel.saveLikeObs) {
            when (it) {
                is SavedViewModel.SaveLikeState.Loading -> {
                    showLoading()
                }
                is SavedViewModel.SaveLikeState.Success -> {
                    hideLoading()
                    viewModel.getRemember()
                }
                is SavedViewModel.SaveLikeState.Failed -> {
                    hideLoading()
                }
            }
        }

        observe(viewModel.userProfileObs) { user ->
            if (user != null) {
                myId = user.id
            }
        }

        observe(viewModel.requestLikePostChangeObs) {
            when (it) {
                is SavedViewModel.RequestLikeState.Loading -> {

                }

                is SavedViewModel.RequestLikeState.Success -> {
                    val position = postAdapter.findPositionAndUpdateItem(it.post)
                    if (position!= -1) {
                        rvPost.findViewHolderForAdapterPosition(position)?.let { holder ->
                            when (holder) {
                                is PostAdapter.PostVH -> {
                                    it.post.isLike?.let { isLike -> holder.updateLikeState(isLike, it.post) }
                                }
                                else -> {}
                            }
                        } ?:kotlin.run {
                            postAdapter.notifyItemChanged(position)
                        }
                    }
                }

                is SavedViewModel.RequestLikeState.Failed -> {
                }
            }
        }
    }
}