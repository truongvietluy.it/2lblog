package com.ausdilap.module.home.home.postoptions

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.DialogFragment
import com.ausdilap.R
import com.ausdilap.ext.hide
import com.ausdilap.ext.setOnDelayClickListener
import com.ausdilap.ext.show
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.bottom_sheet_post_options.*
import kotlinx.android.synthetic.main.bottom_sheet_post_options.view.*

@AndroidEntryPoint
class PostOptionBottomSheet(
    private val isRemember: Boolean? = null,
    private val isFollow: Boolean? = null,
    private val onFollow: (() -> Unit?)? = null,
    private val isHideFollow: Boolean? = null,
    private val onSavePost: (() -> Unit?)? = null,
    private val isHideSave: Boolean? = null,
    private val onDeletePost: (() -> Unit?)? = null,
    private val isHideDelete: Boolean? = null
) : DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.bottom_sheet_post_options, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.apply {
            window?.apply {
                setLayout(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )
                setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                val attribute = attributes
                attribute.gravity = Gravity.BOTTOM
                attributes = attribute
            }
            isCancelable = true
        }

        if (isHideDelete == true) {
            tvDelete.hide()
        } else {
            tvDelete.show()
        }

        if (isHideSave == true) {
            tvSave.hide()
        } else {
            tvSave.show()
        }

        if (isHideFollow == true) {
            tvFollow.hide()
        } else {
            tvFollow.show()
        }

        if (isRemember == true) {
            tvSave.text = "Bỏ lưu bài viết"
        } else {
            tvSave.text = "Lưu bài viết"
        }

        if (isFollow == true) {
            tvFollow.text = "Bỏ theo dõi"
        } else {
            tvFollow.text = "Theo dõi"
        }

        view.tvFollow.setOnDelayClickListener {
            onFollow?.invoke()
            dismiss()
        }

        view.tvSave.setOnDelayClickListener {
            onSavePost?.invoke()
            dismiss()
        }

        view.tvDelete.setOnDelayClickListener {
            onDeletePost?.invoke()
            dismiss()
        }

        view.layoutCancel.setOnDelayClickListener {
            dismiss()
        }

    }

}