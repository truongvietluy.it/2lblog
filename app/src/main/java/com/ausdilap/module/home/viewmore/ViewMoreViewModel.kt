package com.ausdilap.module.home.viewmore

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ausdilap.base.BaseViewModel
import com.ausdilap.data.usecase.authentication.GetUserProfileUseCase
import com.ausdilap.data.usecase.authentication.LogoutUseCase
import com.ausdilap.error.AppError
import com.ausdilap.error.ErrorHandler
import com.ausdilap.error.ErrorUtils
import com.ausdilap.module.auth.model.User
import com.ausdilap.pref.UserPref
import com.ausdilap.utils.SingleLiveEvent
import com.ausdilap.utils.addTo
import com.ausdilap.utils.applyBackgroundStream
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ViewMoreViewModel @Inject constructor(
    private val errorUtils: ErrorUtils,
    private val userPref: UserPref,
    private val getUserProfileUseCase: GetUserProfileUseCase,
    private val logoutUseCase: LogoutUseCase
): BaseViewModel() {

    sealed class LogoutState {
        object Loading : LogoutState()
        object Success : LogoutState()
        class Failed(val message: String) : LogoutState()
    }

    sealed class GetProfileState {
        object Loading : GetProfileState()
        class Success(val user: User) : GetProfileState()
        class Failed(val message: String) : GetProfileState()
    }

    private val _getUserProfileObs = MutableLiveData<GetProfileState>()
    val getUserProfileObs : LiveData<GetProfileState> = _getUserProfileObs

    val onLogoutObs = SingleLiveEvent<LogoutState>()
    val userProfileObs = SingleLiveEvent<User?>()

    fun getUserProfile() {
        _getUserProfileObs.postValue(GetProfileState.Loading)
        getUserProfileUseCase.execute()
            .applyBackgroundStream()
            .subscribe({
                _getUserProfileObs.postValue(GetProfileState.Success(it))
            },{
                val error = ErrorHandler.getError(it, AppError::class.java, errorUtils)
                _getUserProfileObs.postValue(GetProfileState.Failed(error.provideErrorMessage()))
            })
    }

    fun onLogout() {
        onLogoutObs.postValue(LogoutState.Loading)
        logoutUseCase.execute()
            .applyBackgroundStream()
            .subscribe ({
                onLogoutObs.postValue(LogoutState.Success)
            }, {
                onLogoutObs.postValue(LogoutState.Success)
            }).addTo(this)
    }
}