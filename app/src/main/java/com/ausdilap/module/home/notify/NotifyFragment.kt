package com.ausdilap.module.home.notify

import android.os.Bundle
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.ausdilap.R
import com.ausdilap.base.BaseFragment
import com.ausdilap.ext.setVisible
import com.ausdilap.module.home.home.HomeFragment
import com.ausdilap.utils.observe
import com.ausdilap.utils.showToast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.notify_fragment.*
import kotlinx.android.synthetic.main.notify_fragment.viewLoading

@AndroidEntryPoint
class NotifyFragment : BaseFragment<NotifyViewModel>() {

    override val viewModel: NotifyViewModel by viewModels()

    override fun getLayoutId(): Int = R.layout.notify_fragment

    private lateinit var notifyAdapter: NotifyAdapter

    override fun initialize() {
        super.initialize()
        initData()
        initAdapter()
        initRV()

        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true ) {
                override fun handleOnBackPressed() {
                    findNavController().popBackStack()
                }
            }

        activity?.onBackPressedDispatcher?.addCallback(this, callback)
    }

    private fun initData() {
        viewModel.getNotify()
    }

    private fun initAdapter() {
        notifyAdapter = NotifyAdapter(
            onItemClickListener = { notify ->
                val bundle = Bundle()
                bundle.putString(HomeFragment.POST_ID, notify.postId)
                findNavController().navigate(R.id.goToPostDetailFragment, bundle)
                notify.id?.let { id -> viewModel.seenNotify(id) }
            }
        )
    }

    private fun initRV() {
        rvNotification.apply {
            adapter = notifyAdapter
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        }
    }

    override fun observeViewModel() {
        super.observeViewModel()
        observe(viewModel.getNotifyObs) {
            when (it) {
                is NotifyViewModel.GetNotifyState.Loading -> {
                    viewLoading.setVisible(notifyAdapter.itemCount == 0)
                }

                is NotifyViewModel.GetNotifyState.Success -> {
                    viewLoading.setVisible(false)
                    notifyAdapter.updateNotify(it.listNotify)
                }

                is NotifyViewModel.GetNotifyState.Failed -> {
                    viewLoading.setVisible(false)
                    activity?.showToast(it.message)
                }
            }
        }

        observe(viewModel.seenNotifyObs) {
            when (it) {
                is NotifyViewModel.SeenNotifyState.Loading -> {

                }

                is NotifyViewModel.SeenNotifyState.Success -> {

                }

                is NotifyViewModel.SeenNotifyState.Failed -> {
                    activity?.showToast(it.message)
                }
            }
        }
    }
}