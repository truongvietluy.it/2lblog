package com.ausdilap.module.home.post

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ausdilap.R
import com.ausdilap.base.BaseAdapter
import com.ausdilap.ext.loadImageUrl
import com.ausdilap.ext.setOnDelayClickListener
import com.ausdilap.module.home.home.ImageAdapter
import kotlinx.android.synthetic.main.item_image.view.*

class AddImageAdapter(
    private val onRemoveItemClickListener: (String) -> Unit
) : BaseAdapter<AddImageAdapter.ImageVH>() {

    private var listImage = mutableListOf<String?>()

    fun updateImage(images: MutableList<String>) {
        listImage.clear()
        listImage.addAll(images)
        notifyDataSetChanged()
    }

    class ImageVH(
        view: View,
        private val onRemoveItemClickListener: (String) -> Unit
    ) : RecyclerView.ViewHolder(view) {
        fun bind(image: String) {
            itemView.apply {
                image.let { ivImage.loadImageUrl(it) }

                ivClose.setOnDelayClickListener {
                    onRemoveItemClickListener.invoke(image)
                }
            }
        }
    }

    fun removeItem(image : String) {
        val index = listImage.indexOf(image)
        listImage.removeAt(index)

        notifyItemRemoved(index)
        notifyItemRangeChanged(index, listImage.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageVH {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_image, parent, false)
        return ImageVH(view, onRemoveItemClickListener)
    }

    override fun onBindViewHolder(holder: ImageVH, position: Int) {
        listImage[position]?.let { holder.bind(it) }
    }

    override fun getItemCount(): Int = listImage.size
}