package com.ausdilap.module.home.home.postdetail

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.DialogFragment
import com.ausdilap.R
import com.ausdilap.ext.setOnDelayClickListener
import kotlinx.android.synthetic.main.bottom_sheet_comment_options.*
import kotlinx.android.synthetic.main.bottom_sheet_post_options.view.*

class CommentOptionsBottomSheet(
    private val onDeleteComment:  (() -> Unit?)? = null
) : DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.bottom_sheet_comment_options, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dialog?.apply {
            window?.apply {
                setLayout(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )
                setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                val attribute = attributes
                attribute.gravity = Gravity.BOTTOM
                attributes = attribute
            }
            isCancelable = true
        }

        tvDelete.setOnDelayClickListener {
            onDeleteComment?.invoke()
            dismiss()
        }

        layoutCancel.setOnDelayClickListener {
            dismiss()
        }
    }

}