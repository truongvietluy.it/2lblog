package com.ausdilap.module.home.notify

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ausdilap.base.BaseViewModel
import com.ausdilap.data.model.Notify
import com.ausdilap.data.usecase.post.GetNotifyUseCase
import com.ausdilap.data.usecase.post.SeenNotifyUseCase
import com.ausdilap.error.AppError
import com.ausdilap.error.ErrorHandler
import com.ausdilap.error.ErrorUtils
import com.ausdilap.utils.addTo
import com.ausdilap.utils.applyBackgroundStream
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class NotifyViewModel @Inject constructor(
    private val getNotifyUseCase: GetNotifyUseCase,
    private val seenNotifyUseCase: SeenNotifyUseCase,
    private val errorUtils: ErrorUtils
) : BaseViewModel() {

    sealed class GetNotifyState {
        object Loading : GetNotifyState()
        class Success(val listNotify: MutableList<Notify>) : GetNotifyState()
        class Failed(val message: String) : GetNotifyState()
    }

    sealed class SeenNotifyState {
        object Loading : SeenNotifyState()
        class Success(val notify: Notify) : SeenNotifyState()
        class Failed(val message: String) : SeenNotifyState()
    }

    private val _getNotifyObs = MutableLiveData<GetNotifyState>()
    val getNotifyObs: LiveData<GetNotifyState> = _getNotifyObs

    private val _seenNotifyObs = MutableLiveData<SeenNotifyState>()
    val seenNotifyObs: LiveData<SeenNotifyState> = _seenNotifyObs

    fun getNotify() {
        _getNotifyObs.postValue(GetNotifyState.Loading)
        getNotifyUseCase.execute()
            .applyBackgroundStream()
            .subscribe({
                _getNotifyObs.postValue(GetNotifyState.Success(it))
            }, {
                val error = ErrorHandler.getError(it, AppError::class.java, errorUtils)
                _getNotifyObs.postValue(GetNotifyState.Failed(error.provideErrorMessage()))
            }).addTo(this)
    }

    fun seenNotify(id: String) {
        _seenNotifyObs.postValue(SeenNotifyState.Loading)
        seenNotifyUseCase.execute(id)
            .applyBackgroundStream()
            .subscribe({
                _seenNotifyObs.postValue(SeenNotifyState.Success(it))
            },{
                val error = ErrorHandler.getError(it, AppError::class.java, errorUtils)
                _seenNotifyObs.postValue(SeenNotifyState.Failed(error.provideErrorMessage()))
            }).addTo(this)
    }

}