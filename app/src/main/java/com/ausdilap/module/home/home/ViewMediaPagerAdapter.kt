package com.ausdilap.module.home.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.ausdilap.R
import com.ausdilap.ext.loadImageUrl


class ViewMediaPagerAdapter(var images : List<String>, var context : Context) : PagerAdapter() {

    lateinit var layoutInflater: LayoutInflater

    override fun getCount(): Int {
       return images.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view.equals(`object`)

    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        layoutInflater = LayoutInflater.from(context)
        var view: View = layoutInflater.inflate(R.layout.item_view_image, container, false)
        val img: ImageView = view.findViewById<ImageView>(R.id.image)
        img.loadImageUrl(images.get(position))
        container.addView(view, 0)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

}