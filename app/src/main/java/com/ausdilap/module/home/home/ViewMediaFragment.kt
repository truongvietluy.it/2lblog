package com.ausdilap.module.home.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.ausdilap.R
import com.ausdilap.ext.setOnDelayClickListener
import kotlinx.android.synthetic.main.fragment_view_media.*

class ViewMediaFragment : Fragment() {

    private val medias by lazy { arguments?.getStringArrayList(MEDIAS).orEmpty() }
    private val mediaSelected by lazy { arguments?.getString(MEDIA_SELECTED) }
    private lateinit var viewMediaPagerAdapter : ViewMediaPagerAdapter

    companion object {
        const val MEDIAS = "medias"
        const val MEDIA_SELECTED = "media_selected"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_view_media, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    findNavController().popBackStack()
                }
            }

        activity?.onBackPressedDispatcher?.addCallback(viewLifecycleOwner, callback)

        ivBack.setOnDelayClickListener {
            findNavController().popBackStack()
        }

        viewMediaPagerAdapter = ViewMediaPagerAdapter(medias, requireContext())
        viewPagerMedia.apply {
            offscreenPageLimit = 10
            adapter = viewMediaPagerAdapter
        }
    }
}