package com.ausdilap.module.home.home.postdetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ausdilap.base.BaseViewModel
import com.ausdilap.data.model.Comment
import com.ausdilap.data.model.Post
import com.ausdilap.data.model.SaveLikeParam
import com.ausdilap.data.parameter.SavePostParameter
import com.ausdilap.data.usecase.post.*
import com.ausdilap.data.usecase.user.FollowUserUseCase
import com.ausdilap.error.AppError
import com.ausdilap.error.ErrorHandler
import com.ausdilap.error.ErrorUtils
import com.ausdilap.module.auth.model.User
import com.ausdilap.module.home.save.SavedViewModel
import com.ausdilap.pref.UserPref
import com.ausdilap.utils.SingleLiveEvent
import com.ausdilap.utils.addTo
import com.ausdilap.utils.applyBackgroundStream
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PostDetailViewModel @Inject constructor(
    private val getPostDetailUseCase: GetPostDetailUseCase,
    private val getCommentUseCase: GetCommentUseCase,
    private val userPref: UserPref,
    private val commentUseCase: CommentUseCase,
    private val saveLikeUseCase: SaveLikeUseCase,
    private val deletePostUseCase: DeletePostUseCase,
    private val deleteCommentUseCase: DeleteCommentUseCase,
    private val followUserUseCase: FollowUserUseCase,
    private val rememberPostUseCase: RememberPostUseCase,
    private val errorUtils: ErrorUtils
) : BaseViewModel() {

    sealed class GetPostDetailState {
        object Loading : GetPostDetailState()
        class Success(val post: Post) : GetPostDetailState()
        class Failed(val message: String) : GetPostDetailState()
    }

    sealed class GetCommentState {
        object Loading : GetCommentState()
        class Success(val comment: MutableList<Comment>) : GetCommentState()
        class Failed(val message: String) : GetCommentState()
    }

    sealed class SaveLikeState {
        object Loading : SaveLikeState()
        class Success(val saveLikeParam: SaveLikeParam) : SaveLikeState()
        class Failed(val message: String) : SaveLikeState()
    }

    sealed class CommentState {
        object Loading : CommentState()
        class Success(val comment: Comment) : CommentState()
        class Failed(val message: String) : CommentState()
    }

    sealed class DeleteCommentState {
        object Loading : DeleteCommentState()
        class Success(val message: String) : DeleteCommentState()
        class Failed(val message: String) : DeleteCommentState()
    }

    sealed class DeletePostState {
        object Loading : DeletePostState()
        class Success(val message: String) : DeletePostState()
        class Failed(val message: String) : DeletePostState()
    }

    sealed class FollowUserState {
        object Loading : FollowUserState()
        class Success(val message: String) : FollowUserState()
        class Failed(val message: String) : FollowUserState()
    }

    sealed class SavePostSate {
        object Loading : SavePostSate()
        class Success(val savePostParameter: SavePostParameter) : SavePostSate()
        class Failed(val message: String) : SavePostSate()
    }

    sealed class RequestLikeState {
        object Loading : RequestLikeState()
        class Failed(val message: String) : RequestLikeState()
        class Success(val post: Post) : RequestLikeState()
    }

    private val _getPostDetailObs = MutableLiveData<GetPostDetailState>()
    val getPostDetailObs: LiveData<GetPostDetailState> = _getPostDetailObs

    private val _getCommentObs = MutableLiveData<GetCommentState>()
    val getCommentObs: LiveData<GetCommentState> = _getCommentObs

    private val _commentObs = MutableLiveData<CommentState>()
    val commentObs: LiveData<CommentState> = _commentObs

    private val _deletePostObs = MutableLiveData<DeletePostState>()
    val deletePostObs: LiveData<DeletePostState> = _deletePostObs

    private val _deleteCommentObs = MutableLiveData<DeleteCommentState>()
    val deleteCommentObs: LiveData<DeleteCommentState> = _deleteCommentObs

    private val _saveLikeObs = MutableLiveData<SaveLikeState>()
    val saveLikeObs: LiveData<SaveLikeState> = _saveLikeObs

    val userProfileObs = SingleLiveEvent<User?>()

    private val _followUserObs = MutableLiveData<FollowUserState>()
    val followUserObs: LiveData<FollowUserState> = _followUserObs

    private val _savePostObs = MutableLiveData<SavePostSate>()
    val savePostObs: LiveData<SavePostSate> = _savePostObs

    private val _requestLikePostChangeObs = MutableLiveData<RequestLikeState>()
    val requestLikePostChangeObs: LiveData<RequestLikeState> = _requestLikePostChangeObs

    fun getUserProfile() {
        userProfileObs.postValue(userPref.getUserProfile())
    }

    fun getPostDetail(id: String) {
        _getPostDetailObs.postValue(GetPostDetailState.Loading)
        getPostDetailUseCase.execute(id)
            .applyBackgroundStream()
            .subscribe({
                _getPostDetailObs.postValue(GetPostDetailState.Success(it))
            }, {
                val error = ErrorHandler.getError(it, AppError::class.java, errorUtils)
                _getPostDetailObs.postValue(GetPostDetailState.Failed(error.provideErrorMessage()))
            }).addTo(this)
    }

    fun getComment(id: String) {
        _getCommentObs.postValue(GetCommentState.Loading)
        getCommentUseCase.execute(id)
            .applyBackgroundStream()
            .subscribe({
                _getCommentObs.postValue(GetCommentState.Success(it))
            }, {
                val error = ErrorHandler.getError(it, AppError::class.java, errorUtils)
                _getCommentObs.postValue(GetCommentState.Failed(error.provideErrorMessage()))
            }).addTo(this)
    }

    fun comment(id: String, content: String) {
        _commentObs.postValue(CommentState.Loading)
        commentUseCase.execute(id, content)
            .applyBackgroundStream()
            .subscribe({
                _commentObs.postValue(CommentState.Success(it))
            }, {
                val error = ErrorHandler.getError(it, AppError::class.java, errorUtils)
                _commentObs.postValue(CommentState.Failed(error.provideErrorMessage()))
            }).addTo(this)
    }

    fun deletePost(id: String) {
        _deletePostObs.postValue(DeletePostState.Loading)
        deletePostUseCase.execute(id)
            .applyBackgroundStream()
            .subscribe({
                _deletePostObs.postValue(DeletePostState.Success(it))
            }, {
                val error = ErrorHandler.getError(it, AppError::class.java, errorUtils)
                _deletePostObs.postValue(DeletePostState.Failed(error.provideErrorMessage()))
            })
    }

    fun deleteComment(id: String) {
        _deleteCommentObs.postValue(DeleteCommentState.Loading)
        deleteCommentUseCase.execute(id)
            .applyBackgroundStream()
            .subscribe({
                _deleteCommentObs.postValue(DeleteCommentState.Success(it))
            }, {
                val error = ErrorHandler.getError(it, AppError::class.java, errorUtils)
                _deleteCommentObs.postValue(DeleteCommentState.Failed(error.provideErrorMessage()))
            })
    }

    fun saveLike(id: String) {
        _saveLikeObs.postValue(SaveLikeState.Loading)
        saveLikeUseCase
            .execute(id)
            .applyBackgroundStream()
            .subscribe({
                _saveLikeObs.postValue(SaveLikeState.Success(it))
            }, {
                val error = ErrorHandler.getError(it, AppError::class.java, errorUtils)
                _saveLikeObs.postValue(
                    SaveLikeState.Failed(error.provideErrorMessage())
                )
            }).addTo(this)
    }

    fun followUser(myId: String, userId: String) {
        _followUserObs.postValue(FollowUserState.Loading)
        followUserUseCase.execute(myId, userId)
            .applyBackgroundStream()
            .subscribe({
                _followUserObs.postValue(FollowUserState.Success(it))
            }, {
                val error = ErrorHandler.getError(it, AppError::class.java, errorUtils)
                _followUserObs.postValue(FollowUserState.Failed(error.provideErrorMessage()))
            })
    }

    fun savePost(userId: String, postId: String, id: String) {
        _savePostObs.postValue(SavePostSate.Loading)
        rememberPostUseCase.execute(userId, postId, id)
            .applyBackgroundStream()
            .subscribe({
                _savePostObs.postValue(SavePostSate.Success(it))
            }, {
                val error = ErrorHandler.getError(it, AppError::class.java, errorUtils)
                _savePostObs.postValue(SavePostSate.Failed(error.provideErrorMessage()))
            })
    }

    fun requestLikePost(post: Post) {
        post.isLike = post.isLike?.not()
        _requestLikePostChangeObs.postValue(RequestLikeState.Success(post))
    }
}