package com.ausdilap.module.home.home.postoptions

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ausdilap.base.BaseViewModel
import com.ausdilap.data.usecase.post.DeletePostUseCase
import com.ausdilap.error.AppError
import com.ausdilap.error.ErrorHandler
import com.ausdilap.error.ErrorUtils
import com.ausdilap.utils.applyBackgroundStream
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PostOptionViewModel @Inject constructor(
    private val deletePostUseCase: DeletePostUseCase,
    private val errorUtils: ErrorUtils
) : BaseViewModel(){
    sealed class DeleteState {
        object Loading : DeleteState()
        class Success(val message: String): DeleteState()
        class Failed(val message: String): DeleteState()
    }

    private val _deletePostObs = MutableLiveData<DeleteState>()
    val deletePostObs : LiveData<DeleteState> = _deletePostObs

    fun deletePost(id: String) {
        _deletePostObs.postValue(DeleteState.Loading)
        deletePostUseCase.execute(id)
            .applyBackgroundStream()
            .subscribe({
                _deletePostObs.postValue(DeleteState.Success(it))
            }, {
                val error = ErrorHandler.getError(it, AppError::class.java, errorUtils)
                _deletePostObs.postValue(DeleteState.Failed(error.provideErrorMessage()))
            })
    }
}