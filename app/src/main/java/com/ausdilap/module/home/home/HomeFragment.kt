package com.ausdilap.module.home.home

import android.content.Intent
import android.os.Bundle
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions
import androidx.recyclerview.widget.LinearLayoutManager
import com.ausdilap.R
import com.ausdilap.base.BaseFragment
import com.ausdilap.ext.loadImageUrl
import com.ausdilap.ext.setOnDelayClickListener
import com.ausdilap.ext.setVisible
import com.ausdilap.module.home.home.postoptions.PostOptionBottomSheet
import com.ausdilap.module.home.post.CreatePostActivity
import com.ausdilap.utils.observe
import com.ausdilap.utils.showToast
import com.ausdilap.utils.singleObserve
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.ivUserAvt

@AndroidEntryPoint
class HomeFragment : BaseFragment<HomeViewModel>() {

    override val viewModel: HomeViewModel by viewModels()

    private lateinit var postAdapter: PostAdapter

    private lateinit var postLayoutManager: LinearLayoutManager

    private var myId: String? = null
    private var listImages = arrayListOf<String>()

    override fun getLayoutId(): Int = R.layout.fragment_home

    override fun initialize() {
        super.initialize()
        initAdapter()
        initRecyclerView()
        initData()
        initEvent()

        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    findNavController().popBackStack()
                }
            }

        activity?.onBackPressedDispatcher?.addCallback(this, callback)
    }

    private fun initEvent() {
        etSharePost.setOnDelayClickListener {
            findNavController().navigate(R.id.goToCreatePostFragment)
        }

        ivUserAvt.setOnDelayClickListener {
            findNavController().navigate(R.id.goToViewMoreFragment)
        }

        tvSearch.setOnDelayClickListener {
            viewModel.getAllPost(etSearch.text.toString())
        }
    }

    private fun initData() {
        viewModel.getAllPost("")
        viewModel.getUserProfile()
    }

    private fun initAdapter() {
        postAdapter = PostAdapter(
            onItemClickListener = { post ->
                val bundle = Bundle()
                bundle.putString(POST_ID, post.id)
                findNavController().navigate(R.id.goToPostDetailFragment, bundle)
            },
            onMoreOptionsClickListener = { post ->
                post.isRemember?.let { isRemember ->
                    PostOptionBottomSheet(
                        isRemember = isRemember,
                        isFollow = post.isFollow,
                        onDeletePost = {
                            post.id?.let { postID -> viewModel.deletePost(postID) }
                        },
                        isHideDelete = (myId != post.userId),
                        onSavePost = {
                            post.userId?.let { userId ->
                                post.id?.let { postId ->
                                    viewModel.savePost(
                                        userId,
                                        postId,
                                        postId
                                    )
                                }
                            }
                        },
                        isHideSave = (false),
                        onFollow = {
                            myId?.let { myId ->
                                post.userId?.let { userID ->
                                    viewModel.followUser(myId, userID)
                                }
                            }
                        },
                        isHideFollow = (myId == post.userId)
                    ).show(parentFragmentManager, PostOptionBottomSheet::class.simpleName)
                }
            },
            onLikeClickListener = { post ->
                post.id?.let { id -> viewModel.saveLike(id) }
                viewModel.requestLikePost(post)
            },
            onFollowUser = { post ->
                myId?.let { myId -> post.userId?.let { userId -> viewModel.followUser(myId, userId) } }
            },
            onViewImageClickListener = { post ->
                post.images?.forEach { image ->
                    image.pathImage?.let { it1 -> listImages.add(it1) }
                }
                val bundle = Bundle()
                bundle.putStringArrayList(ViewMediaFragment.MEDIAS, listImages)
                findNavController().navigate(R.id.goToViewMediaFragment, bundle)
            }
        )
    }

    private fun initRecyclerView() {
        rvPost.apply {
            postLayoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            layoutManager = postLayoutManager
            adapter = postAdapter
        }
    }

    override fun observeViewModel() {
        super.observeViewModel()
        singleObserve(viewModel.getAllPostObs) {
            when (it) {
                is HomeViewModel.GetAllPostState.Loading -> {
                    viewLoading.setVisible(postAdapter.itemCount == 0)
                }

                is HomeViewModel.GetAllPostState.Success -> {
                    viewLoading.setVisible(false)
                    postAdapter.updatePost(it.posts)
                }

                is HomeViewModel.GetAllPostState.Failed -> {
                    viewLoading.setVisible(false)
                    requireContext().showToast(it.message)
                }
            }
        }

        observe(viewModel.saveLikeObs) {
            when (it) {
                is HomeViewModel.SaveLikeState.Loading -> {
                }

                is HomeViewModel.SaveLikeState.Success -> {
                }

                is HomeViewModel.SaveLikeState.Failed -> {
                }
            }
        }

        observe(viewModel.userProfileObs) { user ->
            if (user != null) {
                user.avatar?.let { avatar -> ivUserAvt.loadImageUrl(avatar) }
                myId = user.id
            }
        }

        observe(viewModel.deletePostObs) {
            when (it) {
                is HomeViewModel.DeleteState.Loading -> {
                    showLoading()
                }

                is HomeViewModel.DeleteState.Success -> {
                    hideLoading()
                    viewModel.getAllPost("")
                }

                is HomeViewModel.DeleteState.Failed -> {
                    hideLoading()
                }
            }
        }

        observe(viewModel.savePostObs) {
            when (it) {
                is HomeViewModel.SavePostSate.Loading -> {
                    showLoading()
                }

                is HomeViewModel.SavePostSate.Success -> {
                    hideLoading()
                }

                is HomeViewModel.SavePostSate.Failed -> {
                    hideLoading()
                }
            }
        }

        observe(viewModel.followUserObs) {
            when (it) {
                is HomeViewModel.FollowUserState.Loading -> {
                    showLoading()
                }

                is HomeViewModel.FollowUserState.Success -> {
                    hideLoading()
                    viewModel.getAllPost("")
                }

                is HomeViewModel.FollowUserState.Failed -> {
                    hideLoading()
                    viewModel.getAllPost("")
                }
            }
        }

        observe(viewModel.requestLikePostChangeObs) {
            when (it) {
                is HomeViewModel.RequestLikeState.Loading -> {

                }

                is HomeViewModel.RequestLikeState.Success -> {
                    val position = postAdapter.findPositionAndUpdateItem(it.post)
                    if (position!= -1) {
                        rvPost.findViewHolderForAdapterPosition(position)?.let { holder ->
                            when (holder) {
                                is PostAdapter.PostVH -> {
                                    it.post.isLike?.let { isLike -> holder.updateLikeState(isLike, it.post) }
                                }
                                else -> {}
                            }
                        } ?:kotlin.run {
                            postAdapter.notifyItemChanged(position)
                        }
                    }
                }

                is HomeViewModel.RequestLikeState.Failed -> {
                }
            }
        }
    }

    companion object {
        const val POST_ID = "post_id"
    }
}