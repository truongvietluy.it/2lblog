package com.ausdilap.module.home.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ausdilap.R
import com.ausdilap.base.BaseAdapter
import com.ausdilap.ext.hide
import com.ausdilap.ext.loadImageUrl
import com.ausdilap.ext.setOnDelayClickListener
import kotlinx.android.synthetic.main.item_image.view.*

class ImageAdapter(
    private val onItemClickListener: (String) -> Unit
) : BaseAdapter<ImageAdapter.ImageVH>() {

    private var listImage = mutableListOf<String?>()

    fun updateImage(images: MutableList<String>) {
        listImage.clear()
        listImage.addAll(images)
        notifyDataSetChanged()
    }

    class ImageVH(
        view: View,
        private val onItemClickListener: (String) -> Unit
    ) : RecyclerView.ViewHolder(view) {
        fun bind(image: String) {
            itemView.apply {
                image.let { ivImage.loadImageUrl(it) }

                setOnDelayClickListener {
                    onItemClickListener.invoke(image)
                }

                ivClose.hide()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageVH {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_image, parent, false)
        return ImageVH(view, onItemClickListener)
    }

    override fun onBindViewHolder(holder: ImageVH, position: Int) {
        listImage[position]?.let { holder.bind(it) }
    }

    override fun getItemCount(): Int = listImage.size
}