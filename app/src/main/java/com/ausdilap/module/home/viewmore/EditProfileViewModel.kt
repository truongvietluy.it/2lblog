package com.ausdilap.module.home.viewmore

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ausdilap.base.BaseViewModel
import com.ausdilap.data.parameter.EditProfileParam
import com.ausdilap.data.usecase.user.EditProfileUseCase
import com.ausdilap.error.AppError
import com.ausdilap.error.ErrorHandler
import com.ausdilap.error.ErrorUtils
import com.ausdilap.module.auth.model.User
import com.ausdilap.pref.UserPref
import com.ausdilap.utils.SingleLiveEvent
import com.ausdilap.utils.addTo
import com.ausdilap.utils.applyBackgroundStream
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.subjects.PublishSubject
import javax.inject.Inject

@HiltViewModel
class EditProfileViewModel @Inject constructor(
    private val editProfileUseCase: EditProfileUseCase,
    private val errorUtils: ErrorUtils,
    private val userPref: UserPref
) : BaseViewModel() {

    sealed class EditProfileState {
        object Loading : EditProfileState()
        class Success(val message: String) : EditProfileState()
        class Failed(val message: String) : EditProfileState()
    }

    private val avtObs: PublishSubject<Uri> = PublishSubject.create()
    private val userNameObs: PublishSubject<String> = PublishSubject.create()
    private val emailObs: PublishSubject<String> = PublishSubject.create()
    private lateinit var avt: Uri
    private var userName: String? = null
    private var email: String? = null

    private val _editProfileObs = MutableLiveData<EditProfileState>()
    val editProfileObs: LiveData<EditProfileState> = _editProfileObs

    val userProfileObs = SingleLiveEvent<User?>()

    fun getUserProfile() {
        userProfileObs.postValue(userPref.getUserProfile())
    }

    fun editProfile() {
        onEditProfile(EditProfileParam(email.orEmpty(), userName.orEmpty(), avt))
    }

    private fun onEditProfile(credential: EditProfileParam) {
        _editProfileObs.postValue(EditProfileState.Loading)
        editProfileUseCase.execute(credential)
            .applyBackgroundStream()
            .subscribe({
                _editProfileObs.postValue(EditProfileState.Success(it))
            }, {
                val error = ErrorHandler.getError(it, AppError::class.java, errorUtils)
                _editProfileObs.postValue(EditProfileState.Failed(error.provideErrorMessage()))
            }).addTo(this)
    }

    fun onAvtChange(value: Uri) {
        avt = value
        avtObs.onNext(avt)
    }

    fun onUserNameChange(value: String) {
        userName = value
        userNameObs.onNext(userName)
    }

    fun onEmailChange(value: String) {
        email = value
        emailObs.onNext(email)
    }
}