package com.ausdilap.module.home.post

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ausdilap.base.BaseViewModel
import com.ausdilap.data.model.Post
import com.ausdilap.data.parameter.CreatePostParam
import com.ausdilap.data.usecase.post.CreatePostUseCase
import com.ausdilap.error.AppError
import com.ausdilap.error.ErrorHandler
import com.ausdilap.error.ErrorUtils
import com.ausdilap.utils.addTo
import com.ausdilap.utils.applyBackgroundStream
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class CreatePostViewModel @Inject constructor(
    private val createPostUseCase: CreatePostUseCase,
    private val errorUtils: ErrorUtils
) : BaseViewModel() {

    sealed class CreatePostState {
        object Loading : CreatePostState()
        class Success(val post: Post) : CreatePostState()
        class Failed(val message: String) : CreatePostState()
    }

    private val _createPostObs = MutableLiveData<CreatePostState>()
    val createPostObs: LiveData<CreatePostState> = _createPostObs

    fun requestCreatePost(address: String, description: String, images: MutableList<Uri>) {
        if (address.isNullOrEmpty() || description.isNullOrEmpty() || images.isEmpty()) {
            _createPostObs.postValue(CreatePostState.Failed("Hãy nhập đầy đủ thông tin"))
        } else {
            createPost(CreatePostParam(images, "Du lich", address, description, "10202.12", "10202.12"))
        }
    }

    private fun createPost(credential: CreatePostParam) {
        _createPostObs.postValue(CreatePostState.Loading)
        createPostUseCase.execute(credential)
            .applyBackgroundStream()
            .subscribe({
                _createPostObs.postValue(CreatePostState.Success(it))
            }, {
                val error = ErrorHandler.getError(it, AppError::class.java, errorUtils)
                _createPostObs.postValue(CreatePostState.Failed(error.provideErrorMessage()))
            }).addTo(this)
    }
}