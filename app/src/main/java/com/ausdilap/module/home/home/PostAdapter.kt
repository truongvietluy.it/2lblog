package com.ausdilap.module.home.home

import android.annotation.SuppressLint
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ausdilap.R
import com.ausdilap.base.BaseAdapter
import com.ausdilap.data.model.Post
import com.ausdilap.ext.*
import com.ausdilap.widget.GridSpacingItemDecoration
import kotlinx.android.synthetic.main.item_post.view.*

class PostAdapter(
    private val onItemClickListener: (Post) -> Unit,
    private val onMoreOptionsClickListener: (Post) -> Unit,
    private val onLikeClickListener: (Post) -> Unit,
    private val onViewImageClickListener: (Post) -> Unit,
    private val onFollowUser: (Post) -> Unit
) : BaseAdapter<PostAdapter.PostVH>() {

    private var listPost = mutableListOf<Post>()

    class PostVH(
        view: View,
        private val onItemClickListener: (Post) -> Unit,
        private val onMoreOptionsClickListener: (Post) -> Unit,
        private val onLikeClickListener: (Post) -> Unit,
        private val onViewImageClickListener: (Post) -> Unit,
        private val onFollowUser: (Post) -> Unit
    ) : RecyclerView.ViewHolder(view) {
        private lateinit var imageAdapter: ImageAdapter
        private var listImages = mutableListOf<String>()
        private lateinit var userId: String

        @SuppressLint("SetTextI18n")
        fun bind(post: Post) {
            itemView.apply {

                initRecyclerView()

                post.user?.avatar?.let { ivUserAvt.loadImageUri(Uri.parse(it)) }
                tvUserName.text = post.user?.name
                tvDate.text = post.createdAt
                tvAddress.text = post.address
                tvDescription.text = post.description
                val countLike = post.numLike?.toInt()?.minus(1).toString()
                tvCountLike.hide()
                if (post.isLike == true) {
//                    tvCountLike.text = "Bạn và $countLike người khác yêu thích bài viết"
                    tvLike.text = "Đã yêu thích"
                } else {
//                    tvCountLike.text = "$countLike người khác yêu thích bài viết"
                    tvLike.text = "Yêu thích"
                }
                tvComment.text = "Bình luận"
                if (post.userId == post.user?.id) {
                    tvFollow.hide()
                } else {
                    tvFollow.show()
                }
                if (post.isFollow == true) {
                    tvFollow.text = "Bỏ theo dõi"
                } else {
                    tvFollow.text = "Theo dõi"
                }

                post.images?.forEach {
                    it.pathImage?.let { it1 -> listImages.add(it1) }
                }

                imageAdapter.updateImage(listImages)

                post.isLike?.let { setLikeState(it) }
                llLike.setOnDelayClickListener {
                    onLikeClickListener.invoke(post)
                }

                ivViewMore.setOnDelayClickListener {
                    onMoreOptionsClickListener.invoke(post)
                }

                tvFollow.setOnDelayClickListener {
                    onFollowUser.invoke(post)
                }

                setOnDelayClickListener {
                    onItemClickListener.invoke(post)
                }

                rvImage.setOnDelayClickListener {
                    onViewImageClickListener.invoke(post)
                }
            }
        }

        fun setUserId(userId: String) {
            this.userId = userId
        }

        private fun initRecyclerView() {
            itemView.rvImage.apply {
                imageAdapter = ImageAdapter(
                    onItemClickListener = {}
                )
                adapter = imageAdapter
                addItemDecoration(GridSpacingItemDecoration(2, 10f.toPx(context), false))
                layoutManager = GridLayoutManager(context, 2, LinearLayoutManager.VERTICAL, false)
            }
        }

        @SuppressLint("SetTextI18n")
        fun updateLikeState(isLike: Boolean, post: Post) {
            with(itemView) {
                ivLike.imageDrawable(
                    if (isLike) R.drawable.ic_liked else R.drawable.ic_like
                )

                val countLike = post.numLike?.toInt()?.minus(1).toString()
                if (post.isLike == true) {
//                    tvCountLike.text = "Bạn và $countLike người khác yêu thích bài viết"
                    tvLike.text = "Đã yêu thích"
                } else {
//                    tvCountLike.text = "$countLike người khác yêu thích bài viết"
                    tvLike.text = "Yêu thích"
                }
            }
        }

        private fun setLikeState(isLike: Boolean) {
            with(itemView) {
                ivLike.imageDrawable(
                    if (isLike) R.drawable.ic_liked else R.drawable.ic_like
                )
            }
        }
    }

    fun findPositionAndUpdateItem(post: Post): Int {
        var position = -1
        listPost.forEachIndexed { i, p ->
            if (p.id == post.id) {
                p.isLike = post.isLike
                p.numLike = post.numLike?.toInt()?.plus(1).toString()
                position = i
                return@forEachIndexed
            }
        }
        return position
    }

    fun updateLikeState(postId: String, isLiked: Boolean) {
        listPost.forEachIndexed { index, post ->
            if (post.id == postId) {
                post.isLike = isLiked
                notifyItemChanged(index)
            }
        }
    }

    fun updatePost(posts: List<Post>) {
        listPost.clear()
        listPost.addAll(posts)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostVH {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_post, parent, false)
        return PostVH(
            view,
            onItemClickListener,
            onMoreOptionsClickListener,
            onLikeClickListener,
            onViewImageClickListener,
            onFollowUser
        )
    }

    override fun onBindViewHolder(holder: PostVH, position: Int) {
        holder.bind(listPost[position])
    }

    override fun getItemCount(): Int = listPost.size
}