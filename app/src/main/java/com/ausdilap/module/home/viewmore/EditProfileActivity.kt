package com.ausdilap.module.home.viewmore

import android.net.Uri
import androidx.activity.viewModels
import androidx.core.widget.doOnTextChanged
import com.ausdilap.R
import com.ausdilap.base.BaseActivity
import com.ausdilap.ext.loadImageUrl
import com.ausdilap.ext.setOnDelayClickListener
import com.ausdilap.utils.CameraUtils
import com.ausdilap.utils.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.activity_edit_profile.ivUserAvt
import kotlinx.android.synthetic.main.activity_edit_profile.tvEmail

@AndroidEntryPoint
class EditProfileActivity : BaseActivity<EditProfileViewModel>() {

    override val viewModel: EditProfileViewModel by viewModels()

    override fun getLayoutId(): Int {
        return R.layout.activity_edit_profile
    }

    override fun initialize() {
        super.initialize()
        initData()
    }

    private fun initData() {
        viewModel.getUserProfile()
    }

    override fun observeViewModel() {
        super.observeViewModel()

        observe(viewModel.userProfileObs) {
            if (it != null) {
                it.avatar?.let { it1 -> ivUserAvt.loadImageUrl(it1) }
            }
            etUserName.hint = it?.name
            tvEmail.text = it?.email
            it?.email?.let { it1 -> viewModel.onEmailChange(it1) }
        }

        observe(viewModel.editProfileObs) {
            when(it) {
                is EditProfileViewModel.EditProfileState.Loading -> {
                    showLoading()
                }

                is EditProfileViewModel.EditProfileState.Success -> {
                    hideLoading()
                    finish()
                }

                is EditProfileViewModel.EditProfileState.Failed -> {
                    hideLoading()
                    finish()
                }
            }
        }
    }

    override fun setEventListener() {
        super.setEventListener()

        flEdit.setOnDelayClickListener {
            requestPermissionForTakeImage {
                CameraUtils.chooseSinglePhoto(this) { path ->
                    viewModel.onAvtChange(Uri.parse(path))
                    ivUserAvt.setImageURI(Uri.parse(path))
                }
            }
        }

        etUserName.doOnTextChanged { text, _, _, _ ->
            viewModel.onUserNameChange(text.toString())
        }

        tvEdit.setOnDelayClickListener {
            viewModel.editProfile()
        }

        ivBack.setOnDelayClickListener {
            finish()
        }
    }
}