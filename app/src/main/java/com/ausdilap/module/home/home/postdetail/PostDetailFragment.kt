package com.ausdilap.module.home.home.postdetail

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.OnBackPressedCallback
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.ausdilap.R
import com.ausdilap.base.BaseFragment
import com.ausdilap.data.model.Comment
import com.ausdilap.data.model.Post
import com.ausdilap.ext.imageDrawable
import com.ausdilap.ext.loadImageUrl
import com.ausdilap.ext.setOnDelayClickListener
import com.ausdilap.ext.toPx
import com.ausdilap.module.home.home.HomeFragment
import com.ausdilap.module.home.home.ImageAdapter
import com.ausdilap.module.home.home.ViewMediaFragment
import com.ausdilap.module.home.home.postoptions.PostOptionBottomSheet
import com.ausdilap.utils.observe
import com.ausdilap.utils.showToast
import com.ausdilap.widget.GridSpacingItemDecoration
import com.ausdilap.widget.SpacingItemDecoration
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_post_detail.*

@AndroidEntryPoint
class PostDetailFragment : BaseFragment<PostDetailViewModel>() {
    override val viewModel: PostDetailViewModel by viewModels()

    private val postId: String? by lazy { arguments?.getString(HomeFragment.POST_ID) }

    private lateinit var imageAdapter: ImageAdapter
    private lateinit var commentAdapter: CommentAdapter

    private var listImages = arrayListOf<String>()
    private var listComment = mutableListOf<Comment>()
    private lateinit var post: Post
    private var myId: String? = null

    override fun getLayoutId(): Int = R.layout.fragment_post_detail

    override fun initialize() {
        super.initialize()
        postId?.let {
            viewModel.getPostDetail(it)
            viewModel.getComment(it)
        }
        viewModel.getUserProfile()
        initAdapter()
        initRV()

        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    findNavController().popBackStack()
                }
            }

        activity?.onBackPressedDispatcher?.addCallback(this, callback)
    }

    private fun initAdapter() {
        imageAdapter = ImageAdapter(
            onItemClickListener = {
                val bundle = Bundle()
                bundle.putStringArrayList(ViewMediaFragment.MEDIAS, listImages)
                findNavController().navigate(R.id.goToViewMediaFragment, bundle)
            }
        )
        commentAdapter = CommentAdapter(
            onOpenCommentOptions = { comment ->
                if (myId == comment.userId) {
                    CommentOptionsBottomSheet(
                        onDeleteComment = {
                            comment.id?.let { id -> viewModel.deleteComment(id) }
                        }
                    ).show(childFragmentManager, CommentOptionsBottomSheet::class.simpleName)
                }
            }
        )
    }

    private fun initRV() {
        rvImage.apply {
            adapter = imageAdapter
            addItemDecoration(GridSpacingItemDecoration(2, 10f.toPx(requireContext()), false))
            layoutManager = GridLayoutManager(context, 2, LinearLayoutManager.VERTICAL, false)
        }

        rvComment.apply {
            adapter = commentAdapter
            addItemDecoration(
                SpacingItemDecoration(
                    spacingPx = 15f.toPx(requireContext()),
                    addStartSpacing = false,
                    addEndSpacing = false
                )
            )
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        }
    }

    override fun observeViewModel() {
        super.observeViewModel()

        observe(viewModel.userProfileObs) { user ->
            if (user != null) {
                myId = user.id
            }
        }

        observe(viewModel.getPostDetailObs) {
            when (it) {
                is PostDetailViewModel.GetPostDetailState.Loading -> {
                }
                is PostDetailViewModel.GetPostDetailState.Success -> {
                    initDetail(it.post)
                    it.post.images?.forEach { image ->
                        image.pathImage?.let { it1 -> listImages.add(it1) }
                    }
                    imageAdapter.updateImage(listImages)
                    post = it.post
                }
                is PostDetailViewModel.GetPostDetailState.Failed -> {
                    activity?.showToast(it.message)
                }
            }
        }

        observe(viewModel.getCommentObs) {
            when (it) {
                is PostDetailViewModel.GetCommentState.Loading -> {
                    showLoading()
                }
                is PostDetailViewModel.GetCommentState.Success -> {
                    hideLoading()
                    commentAdapter.updateComment(it.comment)
                    listComment.addAll(it.comment)
                }
                is PostDetailViewModel.GetCommentState.Failed -> {
                    hideLoading()
                    activity?.showToast(it.message)
                }
            }
        }

        observe(viewModel.commentObs) {
            when (it) {
                is PostDetailViewModel.CommentState.Loading -> {
                    showLoading()
                }
                is PostDetailViewModel.CommentState.Success -> {
                    hideLoading()
                    postId?.let { it1 -> viewModel.getComment(it1) }
                    etComment.setText("")
                }
                is PostDetailViewModel.CommentState.Failed -> {
                    hideLoading()
                    activity?.showToast(it.message)
                }
            }
        }

        observe(viewModel.deleteCommentObs) {
            when (it) {
                is PostDetailViewModel.DeleteCommentState.Loading -> {
                    showLoading()
                }

                is PostDetailViewModel.DeleteCommentState.Success -> {
                    hideLoading()
                    postId?.let { it1 -> viewModel.getComment(it1) }
                }

                is PostDetailViewModel.DeleteCommentState.Failed -> {
                    hideLoading()
                }
            }
        }

        observe(viewModel.saveLikeObs) {
            when (it) {
                is PostDetailViewModel.SaveLikeState.Loading -> {
                }
                is PostDetailViewModel.SaveLikeState.Success -> {

                }
                is PostDetailViewModel.SaveLikeState.Failed -> {
                }
            }
        }

        observe(viewModel.followUserObs) {
            when (it) {
                is PostDetailViewModel.FollowUserState.Loading -> {
                    showLoading()
                }

                is PostDetailViewModel.FollowUserState.Success -> {
                    hideLoading()
                    postId?.let { postId -> viewModel.getPostDetail(postId) }
                }

                is PostDetailViewModel.FollowUserState.Failed -> {
                    hideLoading()
                    postId?.let { postId -> viewModel.getPostDetail(postId) }
                }
            }
        }

        observe(viewModel.deletePostObs) {
            when (it) {
                is PostDetailViewModel.DeletePostState.Loading -> {
                    showLoading()
                }

                is PostDetailViewModel.DeletePostState.Success -> {
                    hideLoading()
                    postId?.let { postId -> viewModel.getPostDetail(postId) }
                    findNavController().popBackStack()
                }
                is PostDetailViewModel.DeletePostState.Failed -> {
                    hideLoading()
                    findNavController().popBackStack()
                }
            }
        }

        observe(viewModel.savePostObs) {
            when (it) {
                is PostDetailViewModel.SavePostSate.Loading -> {
                    showLoading()
                }

                is PostDetailViewModel.SavePostSate.Success -> {
                    hideLoading()
                }

                is PostDetailViewModel.SavePostSate.Failed -> {
                    hideLoading()
                }
            }
        }

        observe(viewModel.requestLikePostChangeObs) {
            when(it) {
                is PostDetailViewModel.RequestLikeState.Loading -> {

                }

                is PostDetailViewModel.RequestLikeState.Success -> {
                    ivLike.imageDrawable(if (post.isLike == true) R.drawable.ic_liked else R.drawable.ic_like)
                    tvLike.text = if (post.isLike == true) "Đã yêu thích" else "Yêu thích"
                }

                is PostDetailViewModel.RequestLikeState.Failed -> {

                }
            }
        }
    }

    override fun setEventListener() {
        super.setEventListener()

        ivClose.setOnDelayClickListener {
            findNavController().popBackStack()
        }

        tvSend.setOnDelayClickListener {
            postId?.let { postId -> viewModel.comment(postId, etComment.text.toString()) }
        }

        ivViewMore.setOnDelayClickListener {
            post.isRemember?.let { isRemember ->
                PostOptionBottomSheet(
                    isRemember = isRemember,
                    isFollow = post.isFollow,
                    onDeletePost = {
                        post.id?.let { postID -> viewModel.deletePost(postID) }
                    },
                    isHideDelete = (myId != post.userId),
                    onSavePost = {
                        post.userId?.let { userId ->
                            postId?.let { postId ->
                                viewModel.savePost(
                                    userId,
                                    postId, postId
                                )
                            }
                        }
                    },
                    isHideSave = (false),
                    onFollow = {
                        myId?.let { myId ->
                            post.userId?.let { userID ->
                                viewModel.followUser(myId, userID)
                            }
                        }
                    },
                    isHideFollow = (myId == post.userId)
                ).show(parentFragmentManager, PostOptionBottomSheet::class.simpleName)
            }
        }

        llLike.setOnDelayClickListener {
            postId?.let { viewModel.saveLike(it) }
            viewModel.requestLikePost(post)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun initDetail(post: Post) {
        post.user?.avatar?.let { ivUserAvt.loadImageUrl(it) }
        tvUserName.text = post.user?.name
        tvDate.text = post.createdAt
        tvAddress.text = post.address
        tvDescription.text = post.description
//        tvLike.text = "${post.numLike} Yêu thích"
        tvLike.text = if (post.isLike == true) "Đã yêu thích" else "Yêu thích"
        tvComment.text = "Bình luận"
        ivLike.imageDrawable(if (post.isLike == true) R.drawable.ic_liked else R.drawable.ic_like)
    }
}