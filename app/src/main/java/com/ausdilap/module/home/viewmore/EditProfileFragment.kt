package com.ausdilap.module.home.viewmore

import android.net.Uri
import android.text.Editable
import androidx.activity.OnBackPressedCallback
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.ausdilap.R
import com.ausdilap.base.BaseFragment
import com.ausdilap.ext.loadImageUrl
import com.ausdilap.ext.setOnDelayClickListener
import com.ausdilap.utils.CameraUtils
import com.ausdilap.utils.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_edit_profile.*

@AndroidEntryPoint
class EditProfileFragment : BaseFragment<EditProfileViewModel>() {
    override val viewModel: EditProfileViewModel by viewModels()

    override fun getLayoutId(): Int {
        return R.layout.fragment_edit_profile
    }

    override fun initialize() {
        super.initialize()
        initData()

        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true ) {
                override fun handleOnBackPressed() {
                    findNavController().popBackStack()
                }
            }

        activity?.onBackPressedDispatcher?.addCallback(this, callback)
    }

    private fun initData() {
        viewModel.getUserProfile()
    }

    override fun observeViewModel() {
        super.observeViewModel()

        observe(viewModel.userProfileObs) { user ->
            if (user != null) {
                user.avatar?.let { avatar -> ivUserAvt.loadImageUrl(avatar) }
            }
            etUserName.text = Editable.Factory.getInstance().newEditable(user?.name)
            tvEmail.text = user?.email
            user?.email?.let { email -> viewModel.onEmailChange(email) }
        }

        observe(viewModel.editProfileObs) {
            when(it) {
                is EditProfileViewModel.EditProfileState.Loading -> {
                    showLoading()
                }

                is EditProfileViewModel.EditProfileState.Success -> {
                    hideLoading()
                    findNavController().popBackStack()
                }

                is EditProfileViewModel.EditProfileState.Failed -> {
                    hideLoading()
                    findNavController().popBackStack()
                }
            }
        }
    }

    override fun setEventListener() {
        super.setEventListener()

        flEdit.setOnDelayClickListener {
            requestPermissionForTakeImage {
                CameraUtils.chooseSinglePhoto(requireContext()) { path ->
                    viewModel.onAvtChange(Uri.parse(path))
                    ivUserAvt.setImageURI(Uri.parse(path))
                }
            }
        }

        etUserName.doOnTextChanged { text, _, _, _ ->
            viewModel.onUserNameChange(text.toString())
        }

        tvEdit.setOnDelayClickListener {
            viewModel.editProfile()
        }

        ivBack.setOnDelayClickListener {
            findNavController().popBackStack()
        }
    }
}