package com.ausdilap.module.home.home.postdetail

import android.annotation.SuppressLint
import android.net.Uri
import androidx.activity.viewModels
import androidx.core.widget.doOnTextChanged
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.ausdilap.R
import com.ausdilap.base.BaseActivity
import com.ausdilap.data.model.Comment
import com.ausdilap.data.model.Post
import com.ausdilap.ext.loadImageUrl
import com.ausdilap.ext.setOnDelayClickListener
import com.ausdilap.ext.toPx
import com.ausdilap.module.home.home.HomeFragment
import com.ausdilap.module.home.home.ImageAdapter
import com.ausdilap.module.home.home.postoptions.PostOptionBottomSheet
import com.ausdilap.utils.observe
import com.ausdilap.utils.showToast
import com.ausdilap.widget.GridSpacingItemDecoration
import com.ausdilap.widget.SpacingItemDecoration
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_post_detail.*
import kotlinx.android.synthetic.main.item_post.view.*

@AndroidEntryPoint
class PostDetailActivity : BaseActivity<PostDetailViewModel>() {

    override val viewModel: PostDetailViewModel by viewModels()

    private val postId: String? by lazy { intent.getStringExtra(HomeFragment.POST_ID) }

    private lateinit var imageAdapter: ImageAdapter
    private lateinit var commentAdapter: CommentAdapter

    private var listImages = mutableListOf<String>()
    private var listComment = mutableListOf<Comment>()

    override fun getLayoutId(): Int {
        return R.layout.activity_post_detail
    }

    override fun initialize() {
        super.initialize()
        postId?.let {
            viewModel.getPostDetail(it)
            viewModel.getComment(it)
        }
        initAdapter()
        initRV()
    }

    private fun initAdapter() {
        imageAdapter = ImageAdapter(
            onItemClickListener = {
            }
        )
        commentAdapter = CommentAdapter(
            onOpenCommentOptions = {
                CommentOptionsBottomSheet(
                    onDeleteComment = {

                    }
                ).show(supportFragmentManager, CommentOptionsBottomSheet :: class.simpleName)
            }
        )
    }

    private fun initRV() {
        rvImage.apply {
            adapter = imageAdapter
            addItemDecoration(GridSpacingItemDecoration(2, 10f.toPx(this@PostDetailActivity), false))
            layoutManager = GridLayoutManager(context, 2, LinearLayoutManager.VERTICAL, false)
        }

        rvComment.apply {
            adapter = commentAdapter
            addItemDecoration(SpacingItemDecoration(spacingPx = 15f.toPx(this@PostDetailActivity), addStartSpacing = false, addEndSpacing = false))
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        }
    }

    override fun observeViewModel() {
        super.observeViewModel()
        observe(viewModel.getPostDetailObs) {
            when (it) {
                is PostDetailViewModel.GetPostDetailState.Loading -> {
                    showLoading()
                }
                is PostDetailViewModel.GetPostDetailState.Success -> {
                    hideLoading()
                    initDetail(it.post)
                    it.post.images?.forEach { image ->
                        image.pathImage?.let { it1 -> listImages.add(it1) }
                    }
                    imageAdapter.updateImage(listImages)
                }
                is PostDetailViewModel.GetPostDetailState.Failed -> {
                    showToast(it.message)
                    hideLoading()
                }
            }
        }

        observe(viewModel.getCommentObs) {
            when(it) {
                is PostDetailViewModel.GetCommentState.Loading -> {
                    showLoading()
                }
                is PostDetailViewModel.GetCommentState.Success -> {
                    hideLoading()
                    commentAdapter.updateComment(it.comment)
                    listComment.addAll(it.comment)

                }
                is PostDetailViewModel.GetCommentState.Failed -> {
                    hideLoading()
                    showToast(it.message)
                }
            }
        }

        observe(viewModel.commentObs) {
            when(it) {
                is PostDetailViewModel.CommentState.Loading -> {
                    showLoading()
                }
                is PostDetailViewModel.CommentState.Success -> {
                    hideLoading()
                    listComment.add(it.comment)
                    commentAdapter.updateComment(listComment)
                }
                is PostDetailViewModel.CommentState.Failed -> {
                    hideLoading()
                    showToast(it.message)
                }
            }
        }
    }

    override fun setEventListener() {
        super.setEventListener()

        ivClose.setOnDelayClickListener {
            finish()
        }

        tvSend.setOnDelayClickListener {
            postId?.let { viewModel.comment(it, etComment.text.toString()) }
        }

        ivViewMore.setOnDelayClickListener {
            PostOptionBottomSheet(
                onDeletePost = {
                    postId?.let { viewModel.deletePost(it) }
                }
            ).show(supportFragmentManager, PostOptionBottomSheet::class.simpleName)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun initDetail(post: Post) {
        post.user?.avatar?.let { ivUserAvt.loadImageUrl(it) }
        tvUserName.text = post.user?.name
        tvDate.text = post.createdAt
        tvAddress.text = post.address
        tvDescription.text = post.description
        tvLike.text = "${post.numLike} Yêu thích"
        tvComment.text = "${post.numComment} Bình luận"
    }
}