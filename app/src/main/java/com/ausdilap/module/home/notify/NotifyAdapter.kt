package com.ausdilap.module.home.notify

import android.annotation.SuppressLint
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ausdilap.R
import com.ausdilap.base.BaseAdapter
import com.ausdilap.data.model.Notify
import com.ausdilap.ext.loadImageUri
import com.ausdilap.ext.setOnDelayClickListener
import com.ausdilap.module.home.home.postdetail.CommentAdapter
import kotlinx.android.synthetic.main.item_notify.view.*

class NotifyAdapter(
    private val onItemClickListener: (Notify) -> Unit
) : BaseAdapter<NotifyAdapter.NotifyVH>() {

    private var listNotify = mutableListOf<Notify>()

    fun updateNotify(notifies: MutableList<Notify>) {
        listNotify.clear()
        listNotify.addAll(notifies)
        notifyDataSetChanged()
    }

    class NotifyVH(
        view: View,
        private val onItemClickListener: (Notify) -> Unit
    ) : RecyclerView.ViewHolder(view) {
        @SuppressLint("SetTextI18n", "ResourceAsColor")
        fun bind(notify: Notify) {
            itemView.apply {
                ivUserAvt.loadImageUri(Uri.parse(notify.user?.avatar))
                if (notify.type == "new_post") {
                    tvContent.text = "${notify.user?.name} đã đăng tải một bài viết"
                } else {
                    tvContent.text = "${notify.user?.name} đã ${notify.type} bài viết của bạn"
                }
                tvDateTime.text = notify.createdAt

                if (notify.isSeen == 1) {
                    setBackgroundColor(resources.getColor(R.color.colorGrayAppBackGround))
                } else {
                    setBackgroundColor(resources.getColor(R.color.color_purple_50))
                }
                setOnDelayClickListener {
                    onItemClickListener.invoke(notify)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotifyVH {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_notify, parent, false)
        return NotifyAdapter.NotifyVH(view, onItemClickListener)
    }

    override fun onBindViewHolder(holder: NotifyVH, position: Int) {
        holder.bind(listNotify[position])
    }

    override fun getItemCount(): Int {
        return listNotify.size
    }
}