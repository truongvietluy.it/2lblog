package com.ausdilap.module.home.viewmore

import android.content.Intent
import android.util.Log
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.ausdilap.R
import com.ausdilap.base.BaseFragment
import com.ausdilap.ext.loadImageUrl
import com.ausdilap.ext.setOnDelayClickListener
import com.ausdilap.module.auth.login.LoginActivity
import com.ausdilap.module.auth.model.User
import com.ausdilap.utils.observe
import com.ausdilap.utils.showToast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.view_more_fragment.*
import kotlin.math.log

@AndroidEntryPoint
class ViewMoreFragment : BaseFragment<ViewMoreViewModel>() {

    override val viewModel: ViewMoreViewModel by viewModels()

    override fun getLayoutId(): Int = R.layout.view_more_fragment

    private var user : User? = null

    override fun initialize() {
        super.initialize()
        viewModel.getUserProfile()

        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true ) {
                override fun handleOnBackPressed() {
                    findNavController().popBackStack()
                }
            }

        activity?.onBackPressedDispatcher?.addCallback(this, callback)
    }

    override fun observeViewModel() {
        super.observeViewModel()

        observe(viewModel.userProfileObs) {
            user = it
            displayData(user)
        }

        observe(viewModel.getUserProfileObs) {
            when (it) {
                is ViewMoreViewModel.GetProfileState.Loading -> {
                    showLoading()
                }

                is ViewMoreViewModel.GetProfileState.Success -> {
                    hideLoading()
                    displayData(it.user)
                }

                is ViewMoreViewModel.GetProfileState.Failed -> {
                    hideLoading()
                }
            }
        }

        observe(viewModel.onLogoutObs) {
            when (it) {
                is ViewMoreViewModel.LogoutState.Loading -> {

                }
                is ViewMoreViewModel.LogoutState.Success -> {
                    startActivity(Intent(requireContext(), LoginActivity::class.java))
                    activity?.finish()
                }
                is ViewMoreViewModel.LogoutState.Failed -> {
                }
            }
        }
    }

    override fun setEventListener() {
        super.setEventListener()

        llEditProfile.setOnDelayClickListener {
            findNavController().navigate(R.id.goToEditProfileFragment)
        }

        llLogout.setOnDelayClickListener {
            viewModel.onLogout()
//            findNavController().navigate(R.id.dialogConfirm)
        }
    }

    private fun displayData(user: User?) {
        if (user!= null) {
            user.avatar?.let { avatar -> ivUserAvt.loadImageUrl(avatar) }
        }
        tvUserName.text = user?.name
        tvEmail.text = user?.email
    }

    override fun onResume() {
        super.onResume()
        viewModel.getUserProfile()
    }
}