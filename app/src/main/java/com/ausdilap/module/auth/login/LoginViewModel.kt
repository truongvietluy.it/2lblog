package com.ausdilap.module.auth.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ausdilap.base.BaseViewModel
import com.ausdilap.data.parameter.LoginEmailParam
import com.ausdilap.data.usecase.authentication.GetUserProfileUseCase
import com.ausdilap.data.usecase.authentication.LoginWithEmailUseCase
import com.ausdilap.error.AppError
import com.ausdilap.error.ErrorHandler
import com.ausdilap.error.ErrorUtils
import com.ausdilap.ext.isEmail
import com.ausdilap.module.auth.model.User
import com.ausdilap.utils.addTo
import com.ausdilap.utils.applyBackgroundStream
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val getLoginWithEmailUseCase: LoginWithEmailUseCase,
    private val getUserProfileUseCase: GetUserProfileUseCase,
    private val errorUtils: ErrorUtils
) : BaseViewModel() {

    sealed class LoginState {
        object Loading : LoginState()
        class LoginError(val message: String) : LoginState()
        class LoginSuccessful(val user: User) : LoginState()

        sealed class ValidateError : LoginState() {
            object EmailRequired : ValidateError()
            object EmailFormat : ValidateError()
            object PasswordRequired : ValidateError()
        }

        class LoginButtonState(val isEnable: Boolean) : LoginState()
    }

    private val emailObs: PublishSubject<String> = PublishSubject.create<String>()
    private val passwordObs: PublishSubject<String> = PublishSubject.create<String>()

    init {
        Observable.combineLatest(emailObs, passwordObs) { email, password ->
            email.isNotEmpty() && password.isNotEmpty()
        }.subscribe({
            _onLoginObs.postValue(LoginState.LoginButtonState(it))
        }, {
            Timber.e(it)
        }).addTo(this)
    }

    private val _onLoginObs = MutableLiveData<LoginState>()
    val onLoginObs: LiveData<LoginState> = _onLoginObs

    private var email: String? = null
    private var password: String? = null

    private fun validateData(): Boolean {
        if (email.isNullOrEmpty()) {
            _onLoginObs.postValue(LoginState.ValidateError.EmailRequired)
            return false
        }

        if (!email.orEmpty().isEmail()) {
            _onLoginObs.postValue(LoginState.ValidateError.EmailFormat)
            return false
        }

        if (password.isNullOrEmpty()) {
            _onLoginObs.postValue(LoginState.ValidateError.PasswordRequired)
            return false
        }
        return true
    }

    fun requestLogin() {
        if (!validateData()) {
            return // data not valid, stop there
        }
        onLoginWithCredential(LoginEmailParam(email.orEmpty(), password.orEmpty()))
    }

    private fun onLoginWithCredential(credential: LoginEmailParam) {
        _onLoginObs.postValue(LoginState.Loading)
        getLoginWithEmailUseCase.execute(credential)
            .applyBackgroundStream()
            .flatMap {
                return@flatMap getUserProfileUseCase.execute()
                    .applyBackgroundStream()
            }
            .subscribe({
                _onLoginObs.postValue(LoginState.LoginSuccessful(it))
            }, {
                val error = ErrorHandler.getError(it, AppError::class.java, errorUtils)
                _onLoginObs.postValue(LoginState.LoginError(error.provideErrorMessage()))
            })
            .addTo(this)
    }

    fun onPasswordChange(value: String) {
        password = value
        passwordObs.onNext(password)
    }

    fun onEmailChange(value: String) {
        email = value
        emailObs.onNext(email)
    }
}