package com.ausdilap.module.auth.register

import android.content.Intent
import androidx.activity.viewModels
import com.ausdilap.R
import com.ausdilap.base.BaseActivity
import com.ausdilap.ext.setOnDelayClickListener
import kotlinx.android.synthetic.main.activity_check_email.*
import java.lang.RuntimeException

class CheckEmailActivity : BaseActivity<CheckEmailViewModel>() {

    override val viewModel: CheckEmailViewModel by viewModels()

    override fun getLayoutId(): Int = R.layout.activity_check_email

    override fun initialize() {
        super.initialize()
    }

    override fun setEventListener() {
        super.setEventListener()
        tvOpenEmailApp.setOnDelayClickListener {
            val gmailIntent : Intent? =
                this.packageManager.getLaunchIntentForPackage("com.google.android.gm")
            if (gmailIntent != null) {
                this.startActivity(gmailIntent)
            }

            try {
                val webIntent = Intent(Intent.ACTION_VIEW)
                webIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                this.startActivity(webIntent)
            } catch (e: RuntimeException) {
                // The url is invalid, maybe missing http://
                e.printStackTrace()
            }
        }
    }
}