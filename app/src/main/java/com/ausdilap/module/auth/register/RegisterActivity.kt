package com.ausdilap.module.auth.register

import android.content.Intent
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import com.ausdilap.R
import com.ausdilap.base.BaseActivity
import com.ausdilap.ext.setOnDelayClickListener
import com.ausdilap.module.auth.login.LoginActivity
import com.ausdilap.utils.observe
import com.ausdilap.utils.showToast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_register.*

@AndroidEntryPoint
class RegisterActivity : BaseActivity<RegisterViewModel>() {

    override val viewModel: RegisterViewModel by viewModels()

    override fun getLayoutId(): Int = R.layout.activity_register

    private var isShowPassword = true
    private var isShowConfirmPassword = true

    override fun initialize() {
        super.initialize()
    }

    override fun setEventListener() {
        super.setEventListener()

        ivShowPassword.setOnDelayClickListener {
            if (isShowPassword) {
                ivShowPassword.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_hide))
                etPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
                etPassword.setSelection(etPassword.length())
                isShowPassword = isShowPassword.not()
            } else {
                ivShowPassword.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_show))
                etPassword.transformationMethod = PasswordTransformationMethod.getInstance()
                etPassword.setSelection(etPassword.length())
                isShowPassword = isShowPassword.not()
            }
        }

        ivShowConfirmPassword.setOnDelayClickListener {
            if (isShowConfirmPassword) {
                ivShowConfirmPassword.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_hide))
                etConfirmPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
                etConfirmPassword.setSelection(etConfirmPassword.length())
                isShowConfirmPassword = isShowConfirmPassword.not()
            } else {
                ivShowConfirmPassword.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_show))
                etConfirmPassword.transformationMethod = PasswordTransformationMethod.getInstance()
                etConfirmPassword.setSelection(etConfirmPassword.length())
                isShowConfirmPassword = isShowConfirmPassword.not()
            }
        }

        etName.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                hideKeyboard(v)
            }
        }

        etEmail.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                hideKeyboard(v)
            }
        }

        etPassword.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                hideKeyboard(v)
            }
        }

        etConfirmPassword.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                hideKeyboard(v)
            }
        }

        etEmail.doOnTextChanged { text, _, _, _ ->
            viewModel.onEmailChange(text.toString())
        }

        etName.doOnTextChanged { text, _, _, _ ->
            viewModel.onNameChange(text.toString())
        }

        etPassword.doOnTextChanged { text, _, _, _ ->
            viewModel.onPasswordChange(text.toString())
        }

        etConfirmPassword.doOnTextChanged { text, _, _, _ ->
            viewModel.onConfirmPasswordChange(text.toString())
        }

        tvRegister.setOnDelayClickListener {
            viewModel.requestRegister()
        }

    }

    override fun observeViewModel() {
        super.observeViewModel()

        observe(viewModel.registerPasswordObs) { state ->
            when (state) {
                is RegisterViewModel.RegisterState.Loading -> {
                    showLoading()
                }
                is RegisterViewModel.RegisterState.Error -> {
                    hideLoading()
                    startActivity(Intent(this, LoginActivity::class.java))
                    finish()
                }
                is RegisterViewModel.RegisterState.Successful -> {
                    hideLoading()
                    startActivity(Intent(this, LoginActivity::class.java))
                    finish()
                }
                is RegisterViewModel.RegisterState.ValidateError.PasswordRequired -> {
                    hideLoading()
                    showToast("Input Password")
                }
                is RegisterViewModel.RegisterState.ValidateError.ConfirmPasswordRequired -> {
                    hideLoading()
                    showToast("Input Confirm Password")
                }
                is RegisterViewModel.RegisterState.ValidateError.ConfirmPasswordIncorrect -> {
                    hideLoading()
                    showToast("Confirm password wrong")
                }
                else -> {
                    hideLoading()
                }
            }
        }
    }

    private fun hideKeyboard(view: View) {
        val inputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }
}