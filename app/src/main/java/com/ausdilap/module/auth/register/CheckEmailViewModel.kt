package com.ausdilap.module.auth.register

import com.ausdilap.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class CheckEmailViewModel @Inject constructor() : BaseViewModel() {
}