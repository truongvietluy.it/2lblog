package com.ausdilap.module.auth.register

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ausdilap.base.BaseViewModel
import com.ausdilap.data.parameter.RegisterParam
import com.ausdilap.data.usecase.authentication.RegisterUseCase
import com.ausdilap.error.AppError
import com.ausdilap.error.ErrorHandler
import com.ausdilap.error.ErrorUtils
import com.ausdilap.utils.addTo
import com.ausdilap.utils.applyBackgroundStream
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(
    private val registerUseCase: RegisterUseCase,
    private val errorUtils: ErrorUtils
): BaseViewModel() {
    sealed class RegisterState {
        object Loading : RegisterState()
        class Error(val message: String) : RegisterState()
        class Successful(val message: String) : RegisterState()
        sealed class ValidateError : RegisterState() {
            object PasswordRequired : ValidateError()
            object ConfirmPasswordRequired : ValidateError()
            object ConfirmPasswordIncorrect : ValidateError()
        }
        class RegisterButtonState(val isEnable: Boolean) : RegisterState()
    }

    private val emailObs: PublishSubject<String> = PublishSubject.create()
    private val nameObs: PublishSubject<String> = PublishSubject.create()
    private val passwordObs: PublishSubject<String> = PublishSubject.create<String>()
    private val confirmPasswordObs: PublishSubject<String> = PublishSubject.create<String>()

    private val _registerPasswordObs = MutableLiveData<RegisterState>()
    val registerPasswordObs: LiveData<RegisterState> = _registerPasswordObs

    private var email: String? = null
    private var name: String? = null
    private var password: String? = null
    private var confirmPassword: String? = null

    private fun validateData(): Boolean {
        if (password.isNullOrEmpty()) {
            _registerPasswordObs.postValue(RegisterState.ValidateError.PasswordRequired)
            return false
        }

        if (confirmPassword.isNullOrEmpty()) {
            _registerPasswordObs.postValue(RegisterState.ValidateError.ConfirmPasswordRequired)
            return false
        }

        if (confirmPassword!=password) {
            _registerPasswordObs.postValue(RegisterState.ValidateError.ConfirmPasswordIncorrect)
        }

        return true
    }

    init {
        Observable.combineLatest(passwordObs, confirmPasswordObs) { password, confirmPassword ->
            password.isNotEmpty() && confirmPassword.isNotEmpty()
        }.subscribe({
            _registerPasswordObs.postValue(RegisterState.RegisterButtonState(it))
        }, {
            Timber.e(it)
        }).addTo(this)
    }

    fun requestRegister() {
        if (!validateData()) {
            return
        }
        onRegisterWithCredential(RegisterParam(name.orEmpty(), email.orEmpty(), password.orEmpty(), confirmPassword.orEmpty()))
    }

    private fun onRegisterWithCredential(credential: RegisterParam) {
        _registerPasswordObs.postValue(RegisterState.Loading)
        registerUseCase.execute(credential)
            .applyBackgroundStream()
            .subscribe({
                _registerPasswordObs.postValue(RegisterState.Successful(it))
            }, {
                val error = ErrorHandler.getError(it, AppError::class.java, errorUtils)
                _registerPasswordObs.postValue(RegisterState.Error(error.provideErrorMessage()))
            })
    }

    fun onEmailChange(value: String) {
        email = value
        emailObs.onNext(email)
    }

    fun onNameChange(value: String) {
        name = value
        nameObs.onNext(name)
    }

    fun onPasswordChange(value: String) {
        password = value
        passwordObs.onNext(password)
    }

    fun onConfirmPasswordChange(value: String) {
        confirmPassword = value
        confirmPasswordObs.onNext(confirmPassword)
    }

}