package com.ausdilap.module.auth.login

import androidx.fragment.app.viewModels
import com.ausdilap.R
import com.ausdilap.base.BaseFragment

class LoginFragment : BaseFragment<LoginViewModel>() {

    override val viewModel: LoginViewModel by viewModels()

    override fun getLayoutId(): Int = R.layout.fragment_login

    init {

    }

}