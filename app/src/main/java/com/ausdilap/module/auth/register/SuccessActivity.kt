package com.ausdilap.module.auth.register

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ausdilap.R

class SuccessActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_success)
    }
}