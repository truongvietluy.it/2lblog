package com.ausdilap.module.auth.login

import android.content.Intent
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import com.ausdilap.R
import com.ausdilap.base.BaseActivity
import com.ausdilap.ext.setOnDelayClickListener
import com.ausdilap.module.auth.forgotpassword.ForgotPasswordActivity
import com.ausdilap.module.auth.register.RegisterActivity
import com.ausdilap.module.home.home.HomeActivity
import com.ausdilap.utils.observe
import com.ausdilap.utils.showToast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_login.*

@AndroidEntryPoint
class LoginActivity : BaseActivity<LoginViewModel>() {

    override val viewModel: LoginViewModel by viewModels()

    override fun getLayoutId(): Int = R.layout.activity_login

    private var isShowPassword = true

    override fun initialize() {
        super.initialize()
    }

    override fun setEventListener() {
        super.setEventListener()

        ivShowPassword.setOnDelayClickListener {
            if (isShowPassword) {
                ivShowPassword.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_hide))
                etPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
                etPassword.setSelection(etPassword.length())
                isShowPassword = isShowPassword.not()
            } else {
                ivShowPassword.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_show))
                etPassword.transformationMethod = PasswordTransformationMethod.getInstance()
                etPassword.setSelection(etPassword.length())
                isShowPassword = isShowPassword.not()
            }
        }

        etEmail.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                hideKeyboard(v)
            }
        }

        etPassword.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                hideKeyboard(v)
            }
        }

        etEmail.doOnTextChanged { text, _, _, _ ->
            viewModel.onEmailChange(text.toString())
        }

        etPassword.doOnTextChanged { text, _, _, _ ->
            viewModel.onPasswordChange(text.toString())
        }

        etPassword.setOnEditorActionListener { _, _, _ ->
            viewModel.requestLogin()
            true
        }

        tvLogin.setOnDelayClickListener {
            viewModel.requestLogin()
        }

        tvRegister.setOnDelayClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
            finish()
        }
    }

    override fun observeViewModel() {
        super.observeViewModel()

        observe(viewModel.onLoginObs) {
            when (it) {
                is LoginViewModel.LoginState.Loading -> {
                    showLoading()
                }
                is LoginViewModel.LoginState.LoginError -> {
                    hideLoading()
                    showToast(it.message)
                }
                is LoginViewModel.LoginState.LoginSuccessful -> {
                    hideLoading()
                    startActivity(Intent(this, HomeActivity::class.java).apply {
                        flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    })
                    finish()
                }
                is LoginViewModel.LoginState.ValidateError.EmailRequired -> {
                    showToast(R.string.please_enter_an_email)
                }
                is LoginViewModel.LoginState.ValidateError.EmailFormat -> {
                    showToast(R.string.does_not_match_any_account)
                }
                is LoginViewModel.LoginState.ValidateError.PasswordRequired -> {
                    showToast(R.string.please_enter_a_password)
                }
                is LoginViewModel.LoginState.LoginButtonState -> {
                    tvLogin.isEnabled = it.isEnable
                }
            }
        }
    }

    private fun hideKeyboard(view: View) {
        val inputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }
}