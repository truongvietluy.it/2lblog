package com.ausdilap.module.auth.forgotpassword

import com.ausdilap.base.BaseViewModel
import com.ausdilap.data.usecase.authentication.ForgotPasswordUseCase
import com.ausdilap.error.AppError
import com.ausdilap.error.ErrorHandler
import com.ausdilap.error.ErrorUtils
import com.ausdilap.ext.isEmail
import com.ausdilap.utils.SingleLiveEvent
import com.ausdilap.utils.addTo
import com.ausdilap.utils.applyBackgroundStream
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ForgotPasswordViewModel @Inject constructor(
    private val errorUtils: ErrorUtils,
    private val forgotPasswordUseCase: ForgotPasswordUseCase
) : BaseViewModel() {
    sealed class State {
        sealed class ValidatorError : State() {
            object EmailRequired : ValidatorError()
            object EmailInvalid : ValidatorError()
        }

        object Loading : State()

        class RetryPasswordFailed(val message: String) : State()

        class RetryPasswordSuccessful(val message: String) : State()

        class SendButtonState(val isEnable: Boolean): State()
    }

    val screenState = SingleLiveEvent<State>()
    var email = ""

    fun emailChange(value: String) {
        email = value
        screenState.postValue(State.SendButtonState(email.isNotEmpty()))
    }

    fun retryPassword() {
        if (email.isEmpty()) {
            screenState.postValue(State.ValidatorError.EmailRequired)
            return
        }
        if (!email.isEmail()) {
            screenState.postValue(State.ValidatorError.EmailInvalid)
            return
        }
        screenState.postValue(State.Loading)
        forgotPasswordUseCase.execute(email)
            .applyBackgroundStream()
            .subscribe({
                screenState.postValue(State.RetryPasswordSuccessful(it))
            }, {
                val error = ErrorHandler.getError(it, AppError::class.java, errorUtils)
                screenState.postValue(State.RetryPasswordFailed(error.provideErrorMessage()))
            })
            .addTo(this)
    }
}