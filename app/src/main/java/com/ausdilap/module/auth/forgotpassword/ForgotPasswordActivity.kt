package com.ausdilap.module.auth.forgotpassword

import android.content.Intent
import androidx.activity.viewModels
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.Observer
import com.ausdilap.R
import com.ausdilap.base.BaseActivity
import com.ausdilap.ext.setOnDelayClickListener
import com.ausdilap.module.auth.register.CheckEmailActivity
import com.ausdilap.utils.showToast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_forgot_password.*

@AndroidEntryPoint
class ForgotPasswordActivity : BaseActivity<ForgotPasswordViewModel>() {

    override val viewModel: ForgotPasswordViewModel by viewModels()

    override fun getLayoutId(): Int = R.layout.activity_forgot_password

    override fun initialize() {
        super.initialize()
    }

    override fun setEventListener() {
        super.setEventListener()

        ivBack.setOnDelayClickListener {
            onBackPressed()
        }

        etEmail.doAfterTextChanged {
            viewModel.emailChange(it.toString())
            layoutEmail.setBackgroundResource(R.drawable.bg_email_input_change)
        }

        tvSend.setOnDelayClickListener {
            viewModel.retryPassword()
        }
    }

    override fun observeViewModel() {
        super.observeViewModel()
        viewModel.screenState.observe(this, screenStateObj)
    }

    private val screenStateObj = Observer<ForgotPasswordViewModel.State> { state ->
        when (state) {
            ForgotPasswordViewModel.State.ValidatorError.EmailRequired -> {
                showToast(getString(R.string.please_enter_an_email))
            }
            ForgotPasswordViewModel.State.ValidatorError.EmailInvalid -> {
                showToast(getString(R.string.please_enter_a_valid_email))
            }
            ForgotPasswordViewModel.State.Loading -> {
                showLoading(false)
            }
            is ForgotPasswordViewModel.State.RetryPasswordFailed -> {
                hideLoading()
                showToast(state.message)
            }
            is ForgotPasswordViewModel.State.RetryPasswordSuccessful -> {
                hideLoading()
                startActivity(Intent(this, CheckEmailActivity::class.java))
            }
            is ForgotPasswordViewModel.State.SendButtonState -> {
                tvSend.isEnabled = state.isEnable
            }
        }
    }
}