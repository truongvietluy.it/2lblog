package com.ausdilap.module.auth.forgotpassword

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ausdilap.R
import com.ausdilap.ext.setOnDelayClickListener
import com.ausdilap.module.auth.login.LoginActivity
import kotlinx.android.synthetic.main.activity_reset_password_successful.*

class ResetPasswordSuccessfulActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password_successful)

        tvDone.setOnDelayClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
    }
}