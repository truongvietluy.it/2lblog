package com.ausdilap.module.auth.forgotpassword

import android.content.Intent
import android.net.Uri
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import com.ausdilap.R
import com.ausdilap.base.BaseActivity
import com.ausdilap.ext.setOnDelayClickListener
import com.ausdilap.module.auth.login.LoginActivity
import com.ausdilap.utils.observe
import com.ausdilap.utils.showToast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_create_new_password.*
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.activity_forgot_password.ivBack

@AndroidEntryPoint
class CreateNewPasswordActivity : BaseActivity<CreateNewPasswordViewModel>() {
    override fun getLayoutId(): Int = R.layout.activity_create_new_password
    override val viewModel: CreateNewPasswordViewModel by viewModels()

    private var isShowPassword = false

    override fun setEventListener() {
        super.setEventListener()

        ivBack.setOnDelayClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
        }

        ivShowPassword.setOnDelayClickListener {
            if (isShowPassword) {
                ivShowPassword.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_show))
                etPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
                etPassword.setSelection(etPassword.length())
                isShowPassword = isShowPassword.not()
            } else {
                ivShowPassword.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_hide))
                etPassword.transformationMethod = PasswordTransformationMethod.getInstance()
                etPassword.setSelection(etPassword.length())
                isShowPassword = isShowPassword.not()
            }
        }

        etPassword.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                hideKeyboard(v)
            }
        }

        etConfirmPassword.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                hideKeyboard(v)
            }
        }

        etPassword.doOnTextChanged { text, _, _, _ ->
            viewModel.onPasswordChange(text.toString())
        }

        etConfirmPassword.doOnTextChanged { text, _, _, _ ->
            viewModel.onConfirmPasswordChange(text.toString())
        }

        tvCreateNewPassword.setOnDelayClickListener {
            viewModel.onTokenChange(getToken().toString())
            viewModel.requestResetPassword()
        }
    }

    override fun observeViewModel() {
        super.observeViewModel()

        observe(viewModel.resetPasswordObs) {
            when (it) {
                is CreateNewPasswordViewModel.ResetPasswordState.Loading -> {
                    showLoading()
                }
                is CreateNewPasswordViewModel.ResetPasswordState.Error -> {
                    hideLoading()
                    startActivity(Intent(this, ResetPasswordSuccessfulActivity::class.java))
                    finish()
                }
                is CreateNewPasswordViewModel.ResetPasswordState.Successful -> {
                    hideLoading()
                    showToast(R.string.reset_password_success)
                    startActivity(Intent(this, ResetPasswordSuccessfulActivity::class.java))
                    finish()
                }
                is CreateNewPasswordViewModel.ResetPasswordState.ValidateError.PasswordRequired -> {
                    hideLoading()
                    showToast(R.string.password_required)
                }
                is CreateNewPasswordViewModel.ResetPasswordState.ValidateError.ConfirmPasswordRequired -> {
                    hideLoading()
                    showToast(R.string.confirm_password_required)
                }
                is CreateNewPasswordViewModel.ResetPasswordState.ValidateError.ConfirmPasswordIncorrect -> {
                    hideLoading()
                    showToast(R.string.confirm_password_incorrect)
                }
                is CreateNewPasswordViewModel.ResetPasswordState.ResetButtonState -> {
                    tvCreateNewPassword.isEnabled = it.isEnable
                }
            }
        }
    }

    fun getToken(): String? {
        val intent = intent
        val action = intent.action
        val data: Uri? = intent.data
        return data?.getQueryParameter("token")
    }

    private fun hideKeyboard(view: View) {
        val inputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }
}