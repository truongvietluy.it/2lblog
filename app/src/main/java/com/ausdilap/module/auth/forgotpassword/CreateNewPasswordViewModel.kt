package com.ausdilap.module.auth.forgotpassword

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ausdilap.base.BaseViewModel
import com.ausdilap.data.parameter.ResetPasswordParam
import com.ausdilap.data.usecase.authentication.ResetPasswordUseCase
import com.ausdilap.error.AppError
import com.ausdilap.error.ErrorHandler
import com.ausdilap.error.ErrorUtils
import com.ausdilap.utils.addTo
import com.ausdilap.utils.applyBackgroundStream
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class CreateNewPasswordViewModel @Inject constructor(
    private val resetPasswordUseCase: ResetPasswordUseCase,
    private val errorUtils: ErrorUtils,
): BaseViewModel() {
    sealed class ResetPasswordState {
        object Loading: ResetPasswordState()
        class Error(val message: String) : ResetPasswordState()
        class Successful(val message: String): ResetPasswordState()
        sealed class ValidateError :  ResetPasswordState() {
            object PasswordRequired : ValidateError()
            object ConfirmPasswordRequired : ValidateError()
            object ConfirmPasswordIncorrect : ValidateError()
        }
        class ResetButtonState(val isEnable: Boolean) : ResetPasswordState()
    }

    private val passwordObs: PublishSubject<String> = PublishSubject.create<String>()
    private val confirmPasswordObs: PublishSubject<String> = PublishSubject.create<String>()
    private val tokenObs: PublishSubject<String> = PublishSubject.create<String>()

    private var password: String? = null
    private var confirmPassword: String? = null
    private var token: String? = null

    private val _resetPasswordObs = MutableLiveData<ResetPasswordState>()
    val resetPasswordObs: LiveData<ResetPasswordState> = _resetPasswordObs

    init {
        Observable.combineLatest(passwordObs, confirmPasswordObs, { password, confirmPassword ->
            password.isNotEmpty() && confirmPassword.isNotEmpty()
        }).subscribe({
            _resetPasswordObs.postValue(ResetPasswordState.ResetButtonState(it))
        }, {
            Timber.e(it)
        }).addTo(this)
    }

    private fun validateData(): Boolean {
        if (password.isNullOrEmpty()) {
            _resetPasswordObs.postValue(ResetPasswordState.ValidateError.PasswordRequired)
            return false
        }

        if (confirmPassword.isNullOrEmpty()) {
            _resetPasswordObs.postValue(ResetPasswordState.ValidateError.ConfirmPasswordRequired)
            return false
        }

        if (confirmPassword!=password) {
            _resetPasswordObs.postValue(ResetPasswordState.ValidateError.ConfirmPasswordIncorrect)
        }

        return true
    }

    fun requestResetPassword() {
        if (!validateData()) {
            return
        }
        onResetPassWordWithCredential(ResetPasswordParam(token.orEmpty(), password.orEmpty(), confirmPassword.orEmpty()))
    }

    private fun onResetPassWordWithCredential(credential: ResetPasswordParam) {
        _resetPasswordObs.postValue(ResetPasswordState.Loading)
        resetPasswordUseCase.execute(credential)
            .applyBackgroundStream()
            .subscribe({
                _resetPasswordObs.postValue(ResetPasswordState.Successful(it))
            }, {
                val error = ErrorHandler.getError(it, AppError::class.java, errorUtils)
                _resetPasswordObs.postValue(ResetPasswordState.Error(error.provideErrorMessage()))
            })
            .addTo(this)
    }

    fun onPasswordChange(value: String) {
        password = value
        passwordObs.onNext(password)
    }

    fun onConfirmPasswordChange(value: String) {
        confirmPassword = value
        confirmPasswordObs.onNext(confirmPassword)
    }

    fun onTokenChange(value: String) {
        token = value
        tokenObs.onNext(token)
    }
}