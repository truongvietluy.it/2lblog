package com.ausdilap.module.splash

import android.content.Intent
import androidx.activity.viewModels
import com.ausdilap.R
import com.ausdilap.base.BaseActivity
import com.ausdilap.module.auth.login.LoginActivity
import com.ausdilap.module.home.home.HomeActivity
import com.ausdilap.utils.observe
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashActivity : BaseActivity<SplashViewModel>() {

    override val viewModel: SplashViewModel by viewModels()

    override fun getLayoutId(): Int = R.layout.activity_splash

    override fun initialize() {
        super.initialize()
        viewModel.initialize()
    }

    override fun observeViewModel() {
        super.observeViewModel()
        observe(viewModel.appNavigationObs) {
            when (it) {
                is SplashViewModel.AppNavigationState.NavigationLogin -> {
                    startActivity(Intent(this, LoginActivity::class.java).apply {
                        flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    })
                }
                is SplashViewModel.AppNavigationState.NavigationHome -> {
                    startActivity(Intent(this, HomeActivity::class.java).apply {
                        flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    })
                }
            }
        }
    }
}