package com.ausdilap.module.splash

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ausdilap.base.BaseViewModel
import com.ausdilap.data.usecase.authentication.GetUserProfileUseCase
import com.ausdilap.pref.UserPref
import com.ausdilap.pref.isLogged
import com.ausdilap.utils.addTo
import com.ausdilap.utils.applyBackgroundStream
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.functions.BiFunction
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor(
    private val getUserProfileUseCase: GetUserProfileUseCase,
    private val userPref: UserPref
) : BaseViewModel() {

    sealed class AppNavigationState {
        object NavigationLogin : AppNavigationState()
        object NavigationHome : AppNavigationState()
    }

    private val _appNavigationObs = MutableLiveData<AppNavigationState>()
    val appNavigationObs: LiveData<AppNavigationState> = _appNavigationObs

    fun initialize() {
        Observable.combineLatest(
            onDelayScreenObs(),
            onUpdateUserProfileObs(),
            BiFunction { t1, t2 -> Pair(t1, t2) }
        )
            .applyBackgroundStream()
            .subscribe({
                if (userPref.isLogged()) {
                    _appNavigationObs.postValue(AppNavigationState.NavigationHome)
                } else {
                    _appNavigationObs.postValue(AppNavigationState.NavigationLogin)
                }
            }, {
                _appNavigationObs.postValue(AppNavigationState.NavigationLogin)
            }).addTo(this)
    }

    private fun onUpdateUserProfileObs(): Observable<Boolean> {
        return if (userPref.getToken() != null) {
            getUserProfileUseCase.execute()
                .map { true }
                .onErrorResumeNext {
                    userPref.clear()
                    Observable.just(true)
                }
        } else {
            Observable.just(true)
        }
    }

    private fun onDelayScreenObs(): Observable<Boolean> {
        return Observable.timer(1000, TimeUnit.MILLISECONDS).map { true }
    }
}