package com.ausdilap.ext

import android.graphics.Bitmap
import android.graphics.Typeface
import android.media.MediaMetadataRetriever
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.TextPaint
import android.text.style.ClickableSpan
import android.text.style.StyleSpan
import android.view.View
import kotlinx.coroutines.runBlocking

fun String.isEmail(): Boolean {
    return android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
}

fun String.isPhone(): Boolean {
    return android.util.Patterns.PHONE.matcher(this)
        .matches() && this.length >= 9 && this.length <= 13
}

fun String.isNotEmptySpace(): Boolean {
    return this.replace(" ", "").isNotEmpty()
}

@Throws(Throwable::class)
fun String.retryVideoFrameFromVideo(callback: ((Bitmap) -> Any)) {
    val value = this
    runBlocking {
        val bitmap: Bitmap?
        var mediaMetadataRetriever: MediaMetadataRetriever? = null
        try {
            mediaMetadataRetriever = MediaMetadataRetriever()
            mediaMetadataRetriever.setDataSource(value, HashMap())
            // mediaMetadataRetriever.setDataSource(videoPath)
            bitmap = mediaMetadataRetriever.frameAtTime
            bitmap?.let { callback(it) }
        } catch (e: Exception) {
            e.printStackTrace()
            throw Throwable("Exception in String.retryVideoFrameFromVideo" + e.message)
        } finally {
            mediaMetadataRetriever?.release()
        }
    }
}

fun String.spannableBuilder(
    highLight: String? = null,
    textSize: Float, color: Int,
    typeface: Int = Typeface.NORMAL
): SpannableStringBuilder {
    val builder = SpannableStringBuilder(this)
    highLight?.let {
        val position = builder.indexOf(highLight, 0)
        if (position != -1) {
            builder.setSpan(object : StyleSpan(typeface) {
                override fun updateDrawState(ds: TextPaint?) {
                    ds?.textSize = textSize
                    ds?.color = color
                    super.updateDrawState(ds)
                }
            }, position, position + highLight.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
    }
    return builder
}

fun String.spannableBuilder2HighLight(
    highLight1: String? = null,
    highLight1Click: (() -> Any)? = null,
    highLight2: String? = null,
    highLight2Click: (() -> Any)? = null,
    textSize: Float, color: Int,
    typeface: Int = Typeface.NORMAL
): SpannableStringBuilder {
    val builder = SpannableStringBuilder(this)
    highLight1?.let {
        val position = builder.indexOf(highLight1, 0)
        if (position != -1) {
            builder.setSpan(object : StyleSpan(typeface) {
                override fun updateDrawState(ds: TextPaint?) {
                    ds?.textSize = textSize
                    ds?.color = color
                    super.updateDrawState(ds)
                }
            }, position, position + highLight1.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            highLight1Click?.let { clicker ->
                builder.setSpan(object : ClickableSpan() {
                    override fun onClick(widget: View) {
                        clicker.invoke()
                    }
                }, position, position + highLight1.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            }
        }
    }
    highLight2?.let {
        val position = builder.indexOf(highLight2, 0)
        if (position != -1) {
            builder.setSpan(object : StyleSpan(typeface) {
                override fun updateDrawState(ds: TextPaint?) {
                    ds?.textSize = textSize
                    ds?.color = color
                    super.updateDrawState(ds)
                }
            }, position, position + highLight2.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            highLight2Click?.let { clicker ->
                builder.setSpan(object : ClickableSpan() {
                    override fun onClick(widget: View) {
                        clicker.invoke()
                    }
                }, position, position + highLight2.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            }
        }
    }
    return builder
}

fun String.spannableBuilder2HighLightV2(
    highLight1: String? = null,
    highLight1Click: (() -> Any)? = null,
    highLight2: String? = null,
    highLight2Click: (() -> Any)? = null,
    textSize: Float,
    color: Int,
    typeface: Typeface = Typeface.DEFAULT
): SpannableStringBuilder {
    val builder = SpannableStringBuilder(this)
    highLight1?.let {
        val position = builder.indexOf(highLight1, 0)
        if (position != -1) {
            builder.setSpan(object : StyleSpan(Typeface.NORMAL) {
                override fun updateDrawState(ds: TextPaint?) {
                    ds?.textSize = textSize
                    ds?.color = color
                    ds?.typeface = typeface
                    super.updateDrawState(ds)
                }
            }, position, position + highLight1.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            highLight1Click?.let { clicker ->
                builder.setSpan(object : ClickableSpan() {
                    override fun onClick(widget: View) {
                        clicker.invoke()
                    }
                }, position, position + highLight1.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            }
        }
    }
    highLight2?.let {
        val position = builder.indexOf(highLight2, 0)
        if (position != -1) {
            builder.setSpan(object : StyleSpan(Typeface.NORMAL) {
                override fun updateDrawState(ds: TextPaint?) {
                    ds?.textSize = textSize
                    ds?.color = color
                    ds?.typeface = typeface
                    super.updateDrawState(ds)
                }
            }, position, position + highLight2.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            highLight2Click?.let { clicker ->
                builder.setSpan(object : ClickableSpan() {
                    override fun onClick(widget: View) {
                        clicker.invoke()
                    }
                }, position, position + highLight2.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            }
        }
    }
    return builder
}

fun String.spannableBuilder3HighLight(
    highLight1: String? = null,
    highLight2: String? = null,
    highLight3: String? = null,
    textSize: Float, color: Int,
    typeface: Int = Typeface.NORMAL
): SpannableStringBuilder {
    val builder = SpannableStringBuilder(this)
    highLight1?.let {
        val position = builder.indexOf(highLight1, 0)
        if (position != -1) {
            builder.setSpan(object : StyleSpan(typeface) {
                override fun updateDrawState(ds: TextPaint?) {
                    ds?.textSize = textSize
                    ds?.color = color
                    super.updateDrawState(ds)
                }
            }, position, position + highLight1.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
    }
    highLight2?.let {
        val position = builder.indexOf(highLight2, 0)
        if (position != -1) {
            builder.setSpan(object : StyleSpan(typeface) {
                override fun updateDrawState(ds: TextPaint?) {
                    ds?.textSize = textSize
                    ds?.color = color
                    super.updateDrawState(ds)
                }
            }, position, position + highLight2.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
    }
    highLight3?.let {
        val position = builder.indexOf(highLight3, 0)
        if (position != -1) {
            builder.setSpan(object : StyleSpan(typeface) {
                override fun updateDrawState(ds: TextPaint?) {
                    ds?.textSize = textSize
                    ds?.color = color
                    super.updateDrawState(ds)
                }
            }, position, position + highLight3.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
    }
    return builder
}

fun String.spannableBuilderAllHighLight(
    highLight: String? = null,
    textSize: Float, color: Int,
    typeface: Int = Typeface.NORMAL
): SpannableStringBuilder {
    val builder = SpannableStringBuilder(this)
    var startIndex = 0
    while (startIndex != -1) {
        highLight?.let {
            val position = builder.indexOf(highLight, if (startIndex == 0) 0 else startIndex + 1)
            if (position != -1) {
                builder.setSpan(object : StyleSpan(typeface) {
                    override fun updateDrawState(ds: TextPaint?) {
                        ds?.textSize = textSize
                        ds?.color = color
                        super.updateDrawState(ds)
                    }
                }, position, position + highLight.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            }
            startIndex = position
        }
    }
    return builder
}

fun String.spannableBuilder3HighLight(
    highLight1: String? = null,
    highLight2: String? = null,
    highLight3: String? = null,
    textSize: Float, color: Int,
    typeface: Typeface = Typeface.DEFAULT
): SpannableStringBuilder {
    val builder = SpannableStringBuilder(this)
    highLight1?.let {
        val position = builder.indexOf(highLight1, 0)
        if (position != -1) {
            builder.setSpan(object : StyleSpan(Typeface.NORMAL) {
                override fun updateDrawState(ds: TextPaint?) {
                    ds?.textSize = textSize
                    ds?.color = color
                    ds?.typeface = typeface
                    super.updateDrawState(ds)
                }
            }, position, position + highLight1.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
    }
    highLight2?.let {
        val position = builder.indexOf(highLight2, 0)
        if (position != -1) {
            builder.setSpan(object : StyleSpan(Typeface.NORMAL) {
                override fun updateDrawState(ds: TextPaint?) {
                    ds?.textSize = textSize
                    ds?.color = color
                    ds?.typeface = typeface
                    super.updateDrawState(ds)
                }
            }, position, position + highLight2.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
    }
    highLight3?.let {
        val position = builder.indexOf(highLight3, 0)
        if (position != -1) {
            builder.setSpan(object : StyleSpan(Typeface.NORMAL) {
                override fun updateDrawState(ds: TextPaint?) {
                    ds?.textSize = textSize
                    ds?.color = color
                    ds?.typeface = typeface
                    super.updateDrawState(ds)
                }
            }, position, position + highLight3.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
    }
    return builder
}

fun String.spannableBuilderV2(
    highLight: String? = null,
    textSize: Float,
    color: Int,
    typeface: Typeface? = null
): SpannableStringBuilder {
    val builder = SpannableStringBuilder(this)
    highLight?.let {
        val position = builder.indexOf(highLight, 0)
        if (position != -1) {
            builder.setSpan(object : StyleSpan(Typeface.NORMAL) {
                override fun updateDrawState(ds: TextPaint?) {
                    ds?.textSize = textSize
                    ds?.color = color
                    ds?.typeface = typeface
                    super.updateDrawState(ds)
                }
            }, position, position + highLight.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
    }
    return builder
}