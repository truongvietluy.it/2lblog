package com.ausdilap.ext

import android.net.Uri
import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat
import com.ausdilap.R
import com.ausdilap.utils.AppUtils
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

fun View.setOnDelayClickListener(method: () -> Unit) {
    this.setOnClickListener {
        this.isEnabled = false
        method.invoke()
        this.postDelayed({
            this.isEnabled = true
        }, 600)
    }
}

fun ImageView.loadImageUrl(link: String) {
    Glide.with(this)
        .load(link)
        .error(R.drawable.ic_logo_empty)
        .placeholder(R.drawable.ic_logo_empty)
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .into(this)
}

fun ImageView.loadImageUri(uri: Uri) {
    Glide.with(this)
        .load(uri)
        .placeholder(R.color.white)
        .centerInside()
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .into(this)
}

fun View.show() {
    visibility = View.VISIBLE
}

fun View.hide() {
    visibility = View.GONE
}

fun View.setVisible(visible: Boolean) {
    if (visible) this.show()
    else this.hide()
}

fun ImageView.imageDrawable(drawable: Int) {
    setImageDrawable(ContextCompat.getDrawable(context, drawable))
}
