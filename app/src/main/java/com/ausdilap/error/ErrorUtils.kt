package com.ausdilap.error

import android.content.Context
import com.ausdilap.R
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

/**
 * Created by pvduc9773 on 3/3/21.
 */
class ErrorUtils @Inject constructor(
    @ApplicationContext val context: Context
) {
    fun getErrorCodeMessage(provideErrorCode: Int): String? {
        return if (provideErrorCode == -1) {
            context.getString(R.string.does_not_match_any_account)
        } else {
            context.getString(R.string.error_code, provideErrorCode)
        }
    }

    fun getErrorServerMessage(): String {
        return context.getString(R.string.error_server)
    }

    fun getErrorAuthorizedMessage(): String {
        return context.getString(R.string.error_authorization)
    }

    fun getErrorNetWorkMessage(): String {
        return context.getString(R.string.error_network)
    }

    fun getErrorIOExceptionMessage(): String {
        return context.getString(R.string.error_data_io)
    }

    fun getErrorJsonSyntaxExceptionMessage(): String {
        return context.getString(R.string.error_data)
    }
}