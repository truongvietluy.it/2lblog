package com.ausdilap.pref

import android.content.Context
import com.ausdilap.module.auth.model.User
import com.google.gson.Gson
import javax.inject.Inject

class UserPref @Inject constructor(context: Context) : BasePref(context, "user-pref") {

    companion object {
        private const val KEY_TOKEN = "key-token"
        private const val KEY_USER_PROFILE = "key-user-info"
    }

    fun getToken(): String? = getString(KEY_TOKEN, null)

    fun saveToken(token: String) = putData(KEY_TOKEN, token)

    fun saveUserProfile(user: User) {
        putData(KEY_USER_PROFILE, Gson().toJson(user))
    }

    fun logout() {
        clear()
    }

    fun getUserProfile(): User? {
        val jsonUserProfile = getString(KEY_USER_PROFILE, null)
        return when {
            jsonUserProfile.isNullOrEmpty() -> {
                null
            }
            else -> {
                Gson().fromJson(jsonUserProfile, User::class.java)
            }
        }
    }
}

fun UserPref.getAccessToken(): String = getToken().orEmpty()

fun UserPref.isLogged(): Boolean = getAccessToken().isNotEmpty()