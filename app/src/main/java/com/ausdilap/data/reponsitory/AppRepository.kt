package com.ausdilap.data.reponsitory

import com.ausdilap.data.service.AppService
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class AppRepository @Inject constructor(
    private val appService: AppService
) {
    fun getHomeData(): Observable<Any> {
        return appService.getHomeData().map { it.data }
    }
}