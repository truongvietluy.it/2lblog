package com.ausdilap.data.reponsitory

import android.net.Uri
import com.ausdilap.data.model.*
import com.ausdilap.data.parameter.SavePostParameter
import com.ausdilap.data.response.BaseResponse
import com.ausdilap.data.service.PostService
import com.ausdilap.pref.UserPref
import com.ausdilap.utils.FileHelper.prepareFilePart
import io.reactivex.rxjava3.core.Observable
import okhttp3.MultipartBody
import retrofit2.http.Multipart
import javax.inject.Inject

class PostRepository @Inject constructor(
    private val postService: PostService,
    private val userPref: UserPref
) : BaseRepository() {

    fun getAllPost(keyword: String): Observable<List<Post>> {
        return postService.getAllPost(
            getBearerToken(userPref.getToken().orEmpty()),
            "1",
            keyword
        ).map { it.data }
    }

    fun createPost(
        images: MutableList<Uri>,
        title: String,
        address: String,
        description: String,
        long: String,
        lat: String
    ): Observable<Post> {
        val requestBody = MultipartBody.Builder().setType(MultipartBody.FORM)

        images.forEach {
            requestBody.addPart(prepareFilePart("images[]", it))
        }

        requestBody.addFormDataPart("title", title)

        requestBody.addFormDataPart("address", address)

        requestBody.addFormDataPart("description", description)

        requestBody.addFormDataPart("_long", long)

        requestBody.addFormDataPart("_lat", lat)

        return postService.createPost(
            getBearerToken(userPref.getToken().orEmpty()),
            requestBody.build().parts
            ).map { it.data }
    }

    fun getPostDetail(id: String): Observable<Post> {
        return postService.getDetailPost(
            getBearerToken(userPref.getToken().orEmpty()),
            id
        ).map { it.data }
    }

    fun updatePost(
        id: String,
        address: String,
        description: String,
        long: String,
        lat: String
    ): Observable<Post> {
        return postService.updatePost(
            getBearerToken(userPref.getToken().orEmpty()),
            id,
            mapOf(
                "address" to address,
                "description" to description,
                "_long" to long,
                "_lat" to lat
            )
        ).map { it.data }
    }

    fun deletePost(id: String): Observable<String> {
        return postService.deletePost(
            getBearerToken(userPref.getToken().orEmpty()),
            id
        ).map { it.data }
    }

    fun rememberPost(userId: String, postId: String, id: String): Observable<SavePostParameter> {
        return postService.rememberPost(
            getBearerToken(userPref.getToken().orEmpty()),
            mapOf(
                "user_id" to userId,
                "post_id" to postId
            ),
            id
        ).map { it.data }
    }

    fun unRememberPost(id: String): Observable<String> {
        return postService.unRememberPost(
            getBearerToken(userPref.getToken().orEmpty()),
            id
        ).map { it.data }
    }

    fun getRememberPost(): Observable<MutableList<Post>> {
        return postService.getRememberPost(
            getBearerToken(userPref.getToken().orEmpty())
        ).map { it.data }
    }

    fun saveLike(id: String, likeType: String = "App\\Post"): Observable<SaveLikeParam> {
        return postService.saveLike(
            getBearerToken(userPref.getToken().orEmpty()),
            id,
            mapOf(
                "like_type" to likeType
            )
        ).map { it.data }
    }

    fun dislike(id: String): Observable<String> {
        return postService.disLike(
            getBearerToken(userPref.getToken().orEmpty()),
            id
        ).map { it.data }
    }

    fun getComment(id: String): Observable<MutableList<Comment>> {
        return postService.getComment(
            getBearerToken(userPref.getToken().orEmpty()),
            id
        ).map { it.data }
    }

    fun comment(id: String, content: String): Observable<Comment> {
        return postService.comment(
            getBearerToken(userPref.getToken().orEmpty()),
            id,
            mapOf(
                "contents" to content
            )
        ).map { it.data }
    }

    fun deleteComment(id: String): Observable<String> {
        return postService.deleteComment(
            getBearerToken(userPref.getToken().orEmpty()),
            id
        ).map { it.data }
    }

    fun getNotify(): Observable<MutableList<Notify>> {
        return postService.getNotify(
            getBearerToken(userPref.getToken().orEmpty())
        ).map { it.data }
    }

    fun seenNotify(id: String): Observable<Notify> {
        return postService.seenNotify(
            getBearerToken(userPref.getToken().orEmpty()),
            id
        ).map { it.data }
    }

}