package com.ausdilap.data.reponsitory

abstract class BaseRepository {
    fun getBearerToken(token: String) = "Bearer $token"
}