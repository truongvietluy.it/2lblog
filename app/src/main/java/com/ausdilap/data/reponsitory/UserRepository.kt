package com.ausdilap.data.reponsitory

import android.net.Uri
import com.ausdilap.data.service.UserService
import com.ausdilap.module.auth.model.User
import com.ausdilap.pref.UserPref
import com.ausdilap.utils.FileHelper
import io.reactivex.rxjava3.core.Observable
import okhttp3.MultipartBody
import javax.inject.Inject

class UserRepository @Inject constructor(
    private val userService: UserService,
    private val userPref: UserPref
) : BaseRepository() {

    fun loginWithEmail(email: String, password: String): Observable<String> {
        return userService.loginWithEmail(
            mapOf(
                "email" to email,
                "password" to password
            )
        ).map { it.data }
    }

    fun getUserProfile(): Observable<User> {
        return userService.getUserProfile(
            getBearerToken(userPref.getToken().orEmpty())
        ).map { it.data }
    }

    fun logout(): Observable<String> {
        return userService.logout(
            getBearerToken(userPref.getToken().orEmpty())
        ).map { it.message }
    }

    fun getRetryPasswordCode(email: String): Observable<String> {
        return userService.forgotPassword(
            mapOf<String,Any>(
                "email" to email,
                "redirect" to "https://dulich-be.stdiohue"
            )
        ).map { it.message }
    }

    fun resetPassword(token: String, password: String, confirmPassword: String): Observable<String> {
        return userService.resetPassword(
            mapOf(
                "token" to token,
                "password" to password,
                "confirmPassword" to confirmPassword
            )
        ).map { it.message }
    }

    fun register(name: String, email: String, password: String, passwordConfirm: String): Observable<String> {
        return userService.register(
            mapOf(
                "name" to name,
                "email" to email,
                "password" to password,
                "password_confirmation" to passwordConfirm
            )
        ).map { it.message }
    }

    fun editProfile(email: String, name: String, avatar: Uri): Observable<String> {
        val requestBody = MultipartBody.Builder().setType(MultipartBody.FORM)

        requestBody.addFormDataPart("email", email)
        requestBody.addFormDataPart("name", name)
        requestBody.addPart(FileHelper.prepareFilePart("avatar", avatar))
        return userService.editProfile(
            getBearerToken(userPref.getToken().orEmpty()),
            requestBody.build().parts
        ).map { it.message }
    }

    fun followUser(myId: String, userId: String): Observable<String> {
        return userService.followUser(
            getBearerToken(userPref.getToken().orEmpty()),
            mapOf(
                "user_id" to myId,
                "follow_user_id" to userId
            )
        ).map { it.message }
    }
}