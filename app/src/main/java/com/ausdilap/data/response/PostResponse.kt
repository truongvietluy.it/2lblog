package com.ausdilap.data.response

import com.ausdilap.data.model.Pagination
import com.ausdilap.data.model.Post

class PostResponse(
    var status: Boolean?,
    var message: String?,
    var data: List<Post>,
    var pagination: Pagination,
    var error: String?
)