package com.ausdilap.data.response

class LoginResponse(
    var status: Boolean?,
    var message: String?,
    var data: String?
)