package com.ausdilap.data.response

class LogoutResponse(
    var status: Boolean?,
    var data: String?,
    var message: String?
)