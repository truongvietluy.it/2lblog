package com.ausdilap.data.response

class BaseResponse<T> {
    var status: Boolean = false
    var message: String? = null
    var data: T? = null
}