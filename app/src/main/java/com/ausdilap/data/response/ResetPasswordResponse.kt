package com.ausdilap.data.response

class ResetPasswordResponse (
    var status: Boolean?,
    var message: String?
    )