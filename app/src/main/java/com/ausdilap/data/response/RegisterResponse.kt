package com.ausdilap.data.response

class RegisterResponse(
    var status: Boolean?,
    var message: String?,
    var data: String?
)