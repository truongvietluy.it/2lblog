package com.ausdilap.data.response

class ForgotPasswordResponse (
    var status: Boolean?,
    var message: String?
)