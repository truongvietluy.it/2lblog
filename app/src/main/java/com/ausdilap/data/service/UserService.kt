package com.ausdilap.data.service

import com.ausdilap.data.response.*
import com.ausdilap.data.response.LogoutResponse
import com.ausdilap.module.auth.model.User
import io.reactivex.rxjava3.core.Observable
import okhttp3.MultipartBody
import retrofit2.http.*

interface UserService {

    @POST("api/login")
    fun loginWithEmail(@Body json: Any): Observable<LoginResponse>

    @GET("api/user/me")
    fun getUserProfile(
        @Header("Authorization") authenticator: String
    ): Observable<BaseResponse<User>>

    @POST("api/logout")
    fun logout(
        @Header("Authorization") authenticator: String
    ): Observable<LogoutResponse>

    @POST("api/send-verify")
    fun forgotPassword(@Body json: Any): Observable<ForgotPasswordResponse>

    @GET("api/reset-password")
    fun resetPassword(@Body json: Any): Observable<ResetPasswordResponse>

    @POST("api/register")
    fun register(@Body json: Any): Observable<RegisterResponse>

    @Multipart
    @POST("api/user/update")
    fun editProfile(
        @Header("Authorization") authenticator: String,
        @Part body: List<MultipartBody.Part>
    ): Observable<BaseResponse<String>>

    @POST("api/user/follow")
    fun followUser(
        @Header("Authorization") authenticator: String,
        @Body json: Any
    ): Observable<BaseResponse<String>>
}