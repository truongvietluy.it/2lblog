package com.ausdilap.data.service

import com.ausdilap.data.response.BaseResponse
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET

/**
 * Created by pvduc9773 on 02/11/2021.
 */
interface AppService {

    @GET("api/v2/mobile/home")
    fun getHomeData(): Observable<BaseResponse<Any>>

}