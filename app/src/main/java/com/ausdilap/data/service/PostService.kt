package com.ausdilap.data.service

import com.ausdilap.data.model.Comment
import com.ausdilap.data.model.Notify
import com.ausdilap.data.model.Post
import com.ausdilap.data.model.SaveLikeParam
import com.ausdilap.data.parameter.SavePostParameter
import com.ausdilap.data.response.BaseResponse
import com.ausdilap.data.response.PostResponse
import io.reactivex.rxjava3.core.Observable
import okhttp3.MultipartBody
import retrofit2.http.*

interface PostService {

    @GET("api/posts")
    fun getAllPost(
        @Header("Authorization") authenticator: String,
        @Query("page") page: String,
        @Query("keyword") keyword: String
    ): Observable<PostResponse>

    @Multipart
    @POST("api/posts/create")
    fun createPost(
        @Header("Authorization") authenticator: String,
        @Part body: List<MultipartBody.Part>
    ): Observable<BaseResponse<Post>>

    @GET("api/posts/{post_id}")
    fun getDetailPost(
        @Header("Authorization") authenticator: String,
        @Path("post_id") id: String
    ): Observable<BaseResponse<Post>>

    @PATCH("api/posts/{post_id}")
    fun updatePost(
        @Header("Authorization") authenticator: String,
        @Path("post_id") id: String,
        @Body json: Any
    ): Observable<BaseResponse<Post>>

    @DELETE("api/posts/{post_id}")
    fun deletePost(
        @Header("Authorization") authenticator: String,
        @Path("post_id") id: String
    ): Observable<BaseResponse<String>>

    @POST("api/posts/{post_id}/remember")
    fun rememberPost(
        @Header("Authorization") authenticator: String,
        @Body json: Any,
        @Path("post_id") id: String
    ): Observable<BaseResponse<SavePostParameter>>

    @DELETE("api/posts/{post_id}/unremember")
    fun unRememberPost(
        @Header("Authorization") authenticator: String,
        @Path("post_id") id: String
    ): Observable<BaseResponse<String>>

    @GET("api/posts/index-remember?page=1")
    fun getRememberPost(
        @Header("Authorization") authenticator: String
    ): Observable<BaseResponse<MutableList<Post>>>

    @POST("api/posts/{post_id}/likes")
    fun saveLike(
        @Header("Authorization") authenticator: String,
        @Path("post_id") id: String,
        @Body json: Any
    ): Observable<BaseResponse<SaveLikeParam>>

    @DELETE("api/posts/{id}/dislike")
    fun disLike(
        @Header("Authorization") authenticator: String,
        @Path("id") id: String
    ): Observable<BaseResponse<String>>

    @GET("api/posts/{post_id}/comments?page=1")
    fun getComment(
        @Header("Authorization") authenticator: String,
        @Path("post_id") id: String
    ): Observable<BaseResponse<MutableList<Comment>>>

    @POST("api/posts/{post_id}/comments")
    fun comment(
        @Header("Authorization") authenticator: String,
        @Path("post_id") id: String,
        @Body json: Any
    ): Observable<BaseResponse<Comment>>

    @DELETE("api/posts/{comment_id}/comments")
    fun deleteComment(
        @Header("Authorization") authenticator: String,
        @Path("comment_id") id: String
    ): Observable<BaseResponse<String>>

    @GET("api/notification?page=1")
    fun getNotify(
        @Header("Authorization") authenticator: String
    ): Observable<BaseResponse<MutableList<Notify>>>

    @PATCH("api/notification/{notify_id}")
    fun seenNotify(
        @Header("Authorization") authenticator: String,
        @Path("notify_id") id: String
    ): Observable<BaseResponse<Notify>>
}