package com.ausdilap.data.model

import android.os.Parcelable
import com.ausdilap.module.auth.model.User
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Post(
    @SerializedName("id")
    var id: String? = null,
    @SerializedName("user_id")
    var userId: String? = null,
    @SerializedName("description")
    var description: String? = null,
    @SerializedName("address")
    var address: String? = null,
    @SerializedName("_long")
    var long: String? = null,
    @SerializedName("_lat")
    var lat: String? = null,
    @SerializedName("is_seen")
    var isSeen: Int? = 0,
    @SerializedName("created_at")
    var createdAt: String? = null,
    @SerializedName("updated_at")
    var updatedAt: String? = null,
    @SerializedName("images")
    var images: List<Image>? = null,
    @SerializedName("author")
    var user: User? = null,
    @SerializedName("num_like")
    var numLike: String? = null,
    @SerializedName("num_comment")
    var numComment: String? = null,
    @SerializedName("is_like")
    var isLike: Boolean? = false,
    @SerializedName("likes")
    var likes: List<User>? = null,
    @SerializedName("title")
    var title: String? = null,
    @SerializedName("is_remember")
    var isRemember: Boolean? = null,
    @SerializedName("is_follow")
    var isFollow: Boolean? = null
) : Parcelable

enum class MediaType(val value: Int) {
    TYPE_IMAGE(0),
    TYPE_VIDEO(1)
}


