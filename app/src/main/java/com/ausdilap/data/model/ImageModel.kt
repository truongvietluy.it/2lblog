package com.ausdilap.data.model

class ImageModel(private val image: Image) {

    fun getPath(): String? = image.path
}