package com.ausdilap.data.model

import android.os.Parcelable
import com.ausdilap.module.auth.model.User
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class Notify(
    @SerializedName("id")
    var id: String? = null,
    @SerializedName("type")
    var type: String? = null,
    @SerializedName("is_seen")
    var isSeen: Int? = null,
    @SerializedName("created_at")
    var createdAt: String? = null,
    @SerializedName("updated_at")
    var updatedAt: String? = null,
    @SerializedName("user_id")
    var userId: String? = null,
    @SerializedName("post_id")
    var postId: String? = null,
    @SerializedName("post")
    var post: Post? = null,
    @SerializedName("user")
    var user: User? = null
) : Parcelable