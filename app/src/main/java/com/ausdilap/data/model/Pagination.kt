package com.ausdilap.data.model

import com.google.gson.annotations.SerializedName

data class Pagination(
    @SerializedName("current_page")
    var currentPage: String? = null,
    @SerializedName("from_record")
    var fromRecord: String? = null,
    @SerializedName("to_record")
    var toRecord: String? = null,
    @SerializedName("total_record")
    var totalRecord: String? = null,
    @SerializedName("record_per_page")
    var recordPerPage: String? = null,
    @SerializedName("total_page")
    var totalPage: String? = null,
)