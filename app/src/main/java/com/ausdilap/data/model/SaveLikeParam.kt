package com.ausdilap.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SaveLikeParam(
    @SerializedName("id")
    var id: String? = null,
    @SerializedName("like_id")
    var likeId: String? = null,
    @SerializedName("user_id")
    var userId: String? = null,
    @SerializedName("like_type")
    var likeType: String? = null,
    @SerializedName("created_at")
    var createdAt: String? = null,
    @SerializedName("updated_at")
    var updatedAt: String? = null,
    @SerializedName("status")
    var status: String? = null,
): Parcelable