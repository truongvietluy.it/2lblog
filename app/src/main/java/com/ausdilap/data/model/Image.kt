package com.ausdilap.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Image(
    @SerializedName("id")
    var id: String? = null,
    @SerializedName("post_id")
    var postId: String? = null,
    @SerializedName("path")
    var path: String? = null,
    @SerializedName("created_at")
    var createdAt: String? = null,
    @SerializedName("updated_at")
    var updatedAt: String? = null,
    @SerializedName("path_image")
    var pathImage: String? = null
): Parcelable
