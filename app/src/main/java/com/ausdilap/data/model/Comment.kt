package com.ausdilap.data.model

import android.os.Parcelable
import com.ausdilap.module.auth.model.User
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class Comment(
    @SerializedName("id")
    var id: String?,
    @SerializedName("parent_id")
    var parentId: String?,
    @SerializedName("post_id")
    var postId: String?,
    @SerializedName("user_id")
    var userId: String?,
    @SerializedName("contents")
    var contents: String?,
    @SerializedName("created_at")
    var createdAt: String?,
    @SerializedName("updated_at")
    var updatedAt: String?,
    @SerializedName("user")
    var user: User
): Parcelable