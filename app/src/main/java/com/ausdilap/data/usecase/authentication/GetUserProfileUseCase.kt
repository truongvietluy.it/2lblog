package com.ausdilap.data.usecase.authentication

import com.ausdilap.data.reponsitory.UserRepository
import com.ausdilap.data.usecase.BaseUseCase
import com.ausdilap.module.auth.model.User
import com.ausdilap.pref.UserPref
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class GetUserProfileUseCase @Inject constructor(
    private val userRepository: UserRepository,
    private val userPref: UserPref
) : BaseUseCase<User>() {
    override fun execute(vararg params: Any): Observable<User> {
        return userRepository.getUserProfile()
            .doOnNext {
                userPref.saveUserProfile(it)
            }
    }
}