package com.ausdilap.data.usecase.authentication

import com.ausdilap.data.parameter.LoginEmailParam
import com.ausdilap.data.reponsitory.UserRepository
import com.ausdilap.data.usecase.BaseUseCase
import com.ausdilap.pref.UserPref
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class LoginWithEmailUseCase @Inject constructor(
    private val userRepository: UserRepository,
    private val userPref: UserPref
) : BaseUseCase<String>() {
    override fun execute(vararg params: Any): Observable<String> {
        val loginEmailParam: LoginEmailParam = params[0] as LoginEmailParam
        return userRepository.loginWithEmail(loginEmailParam.email, loginEmailParam.password)
            .doOnNext {
                userPref.saveToken(it)
            }
    }
}