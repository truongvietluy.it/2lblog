package com.ausdilap.data.usecase.authentication

import com.ausdilap.data.parameter.ResetPasswordParam
import com.ausdilap.data.reponsitory.UserRepository
import com.ausdilap.data.usecase.BaseUseCase
import com.ausdilap.pref.UserPref
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class ResetPasswordUseCase @Inject constructor(
    val userRepository: UserRepository,
    val userPref: UserPref
) : BaseUseCase<String>(){
    override fun execute(vararg params: Any): Observable<String> {
        val resetPasswordParam: ResetPasswordParam = params[0] as ResetPasswordParam
        return userRepository.resetPassword(
            resetPasswordParam.token,
            resetPasswordParam.password,
            resetPasswordParam.confirmPassword
        )}
}