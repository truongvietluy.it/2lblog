package com.ausdilap.data.usecase.post

import com.ausdilap.data.model.Post
import com.ausdilap.data.model.SaveLikeParam
import com.ausdilap.data.reponsitory.PostRepository
import com.ausdilap.data.usecase.BaseUseCase
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class SaveLikeUseCase @Inject constructor(
    private val postRepository: PostRepository
) : BaseUseCase<SaveLikeParam>(){
    override fun execute(vararg params: Any): Observable<SaveLikeParam> {
        val id = params[0] as String
        return postRepository.saveLike(id)
    }
}