package com.ausdilap.data.usecase.authentication

import com.ausdilap.data.reponsitory.UserRepository
import com.ausdilap.data.usecase.BaseUseCase
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class ForgotPasswordUseCase @Inject constructor(
    private val userRepository: UserRepository
) : BaseUseCase<String>() {
    override fun execute(vararg params: Any): Observable<String> {
        return try {
            val email = params[0] as String
            userRepository.getRetryPasswordCode(email)
        } catch (e: Exception) {
            Observable.error(e)
        }
    }
}