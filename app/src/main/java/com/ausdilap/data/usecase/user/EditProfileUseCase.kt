package com.ausdilap.data.usecase.user

import com.ausdilap.data.parameter.EditProfileParam
import com.ausdilap.data.reponsitory.UserRepository
import com.ausdilap.data.usecase.BaseUseCase
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class EditProfileUseCase @Inject constructor(
    private val userRepository: UserRepository
) : BaseUseCase<String>(){
    override fun execute(vararg params: Any): Observable<String> {
        val editProfileParam: EditProfileParam = params[0] as EditProfileParam
        return userRepository.editProfile(
            editProfileParam.email,
            editProfileParam.name,
            editProfileParam.image
        )
    }
}