package com.ausdilap.data.usecase.post

import com.ausdilap.data.model.Post
import com.ausdilap.data.reponsitory.PostRepository
import com.ausdilap.data.usecase.BaseUseCase
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class GetRememberUseCase @Inject constructor(
    val postRepository: PostRepository
) : BaseUseCase<MutableList<Post>>(){
    override fun execute(vararg params: Any): Observable<MutableList<Post>> {
        return postRepository.getRememberPost()
    }
}