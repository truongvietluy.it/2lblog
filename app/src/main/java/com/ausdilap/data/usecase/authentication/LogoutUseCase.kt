package com.ausdilap.data.usecase.authentication

import com.ausdilap.data.reponsitory.UserRepository
import com.ausdilap.data.usecase.BaseUseCase
import com.ausdilap.pref.UserPref
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject


class LogoutUseCase @Inject constructor(
    private val userRepository: UserRepository,
    private val userPref: UserPref
): BaseUseCase<String>() {
    override fun execute(vararg params: Any): Observable<String> {
        return userRepository.logout().doOnNext { userPref.logout() }
    }
}