package com.ausdilap.data.usecase.post

import com.ausdilap.data.reponsitory.PostRepository
import com.ausdilap.data.usecase.BaseUseCase
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class DislikeUseCase @Inject constructor(
    private val postRepository: PostRepository
) : BaseUseCase<String>(){
    override fun execute(vararg params: Any): Observable<String> {
        val id = params[0] as String
        return postRepository.dislike(id)
    }
}