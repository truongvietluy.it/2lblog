package com.ausdilap.data.usecase.post

import com.ausdilap.data.parameter.SavePostParameter
import com.ausdilap.data.reponsitory.PostRepository
import com.ausdilap.data.usecase.BaseUseCase
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class RememberPostUseCase @Inject constructor(
    private val postRepository: PostRepository
) : BaseUseCase<SavePostParameter>() {
    override fun execute(vararg params: Any): Observable<SavePostParameter> {
        val userId = params[0] as String
        val postId = params[1] as String
        val id = params[2] as String
        return postRepository.rememberPost(userId, postId, id)
    }
}