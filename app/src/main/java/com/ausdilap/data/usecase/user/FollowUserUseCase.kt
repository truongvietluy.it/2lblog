package com.ausdilap.data.usecase.user

import com.ausdilap.data.reponsitory.UserRepository
import com.ausdilap.data.usecase.BaseUseCase
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class FollowUserUseCase @Inject constructor(
    private val userRepository: UserRepository
) : BaseUseCase<String>(){
    override fun execute(vararg params: Any): Observable<String> {
        val myId = params[0] as String
        val userId = params[1] as String
        return userRepository.followUser(myId, userId)
    }
}