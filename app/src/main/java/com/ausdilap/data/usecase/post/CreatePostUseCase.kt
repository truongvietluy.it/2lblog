package com.ausdilap.data.usecase.post

import com.ausdilap.data.model.Post
import com.ausdilap.data.parameter.CreatePostParam
import com.ausdilap.data.reponsitory.PostRepository
import com.ausdilap.data.usecase.BaseUseCase
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class CreatePostUseCase @Inject constructor(
    private val postRepository: PostRepository
) : BaseUseCase<Post>() {
    override fun execute(vararg params: Any): Observable<Post> {
        val createPostParam : CreatePostParam = params[0] as CreatePostParam
        return postRepository.createPost(
            createPostParam.images,
            createPostParam.title,
            createPostParam.address,
            createPostParam.description,
            createPostParam.long,
            createPostParam.lat
        )
    }
}