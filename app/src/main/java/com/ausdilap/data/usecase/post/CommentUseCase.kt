package com.ausdilap.data.usecase.post

import com.ausdilap.data.model.Comment
import com.ausdilap.data.reponsitory.PostRepository
import com.ausdilap.data.usecase.BaseUseCase
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class CommentUseCase @Inject constructor(
    private val postRepository: PostRepository
) : BaseUseCase<Comment>() {
    override fun execute(vararg params: Any): Observable<Comment> {
        val content = params[1] as String
        val id = params[0] as String
        return postRepository.comment(id, content)
    }
}