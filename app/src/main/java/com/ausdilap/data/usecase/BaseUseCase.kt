package com.ausdilap.data.usecase

import io.reactivex.rxjava3.core.Observable

abstract class BaseUseCase<T> {
    abstract fun execute(vararg params: Any): Observable<T>
}