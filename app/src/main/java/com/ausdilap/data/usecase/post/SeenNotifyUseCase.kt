package com.ausdilap.data.usecase.post

import com.ausdilap.data.model.Notify
import com.ausdilap.data.reponsitory.PostRepository
import com.ausdilap.data.usecase.BaseUseCase
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class SeenNotifyUseCase @Inject constructor(
    private val postRepository: PostRepository
) : BaseUseCase<Notify>(){
    override fun execute(vararg params: Any): Observable<Notify> {
        val id = params[0] as String
        return postRepository.seenNotify(id)
    }
}