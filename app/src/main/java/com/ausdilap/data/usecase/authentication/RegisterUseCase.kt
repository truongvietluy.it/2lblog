package com.ausdilap.data.usecase.authentication

import com.ausdilap.data.parameter.RegisterParam
import com.ausdilap.data.reponsitory.UserRepository
import com.ausdilap.data.usecase.BaseUseCase
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class RegisterUseCase @Inject constructor(
    private val userRepository: UserRepository
) : BaseUseCase<String> (){
    override fun execute(vararg params: Any): Observable<String> {
        val registerParam: RegisterParam = params[0] as RegisterParam
        return userRepository.register(
            registerParam.name,
            registerParam.email,
            registerParam.password,
            registerParam.passwordConfirm
        )
    }
}