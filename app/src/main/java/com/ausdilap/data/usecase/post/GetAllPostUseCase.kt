package com.ausdilap.data.usecase.post

import com.ausdilap.data.model.Post
import com.ausdilap.data.reponsitory.PostRepository
import com.ausdilap.data.usecase.BaseUseCase
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class GetAllPostUseCase @Inject constructor(
    private val postRepository: PostRepository
) : BaseUseCase<List<Post>>(){
    override fun execute(vararg params: Any): Observable<List<Post>> {
        val keyword = params[0] as String
        return postRepository.getAllPost(keyword)
    }
}