package com.ausdilap.data.usecase.post

import com.ausdilap.data.model.Comment
import com.ausdilap.data.reponsitory.PostRepository
import com.ausdilap.data.usecase.BaseUseCase
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class GetCommentUseCase @Inject constructor(
    private val postRepository: PostRepository
) : BaseUseCase<MutableList<Comment>>() {
    override fun execute(vararg params: Any): Observable<MutableList<Comment>> {
        val id = params[0] as String
        return postRepository.getComment(id)
    }
}