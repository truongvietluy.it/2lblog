package com.ausdilap.data.parameter

import com.google.gson.annotations.SerializedName

class SavePostParameter(
    @SerializedName("id")
    val id: String,
    @SerializedName("user_id")
    val userId: String,
    @SerializedName("post_id")
    val postId: String,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("updated_at")
    val updatedAt: String,
    @SerializedName("status")
    val status: String,
)