package com.ausdilap.data.parameter

class RegisterParam(
    val name: String,
    val email: String,
    val password: String,
    val passwordConfirm: String
)