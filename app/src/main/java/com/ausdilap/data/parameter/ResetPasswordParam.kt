package com.ausdilap.data.parameter

class ResetPasswordParam (val token: String, val password: String, val confirmPassword: String)