package com.ausdilap.data.parameter

class LoginEmailParam(val email: String, val password: String)