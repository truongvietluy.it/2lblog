package com.ausdilap.data.parameter

import android.net.Uri

class EditProfileParam(
    val email: String,
    val name: String,
    val image: Uri
)