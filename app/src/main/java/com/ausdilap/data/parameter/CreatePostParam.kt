package com.ausdilap.data.parameter

import android.net.Uri

class CreatePostParam(
    val images: MutableList<Uri>,
    val title: String,
    val address: String,
    val description: String,
    val long: String,
    val lat: String
)