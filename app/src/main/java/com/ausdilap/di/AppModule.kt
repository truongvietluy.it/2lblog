package com.ausdilap.di

import android.content.Context
import com.ausdilap.pref.UserPref
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun userPref(@ApplicationContext context: Context): UserPref {
        return UserPref(context)
    }

}