package com.ausdilap.di

import com.ausdilap.data.service.AppService
import com.ausdilap.data.service.PostService
import com.ausdilap.data.service.UserService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.create

/**
 * Created by pvduc9773 on 02/11/2021.
 */
@Module
@InstallIn(SingletonComponent::class)
object ServiceModule {

    @Provides
    fun provideUserService(
        @NetworkModule.RetrofitBuilder retrofit: Retrofit
    ): UserService {
        return retrofit.create(UserService::class.java)
    }

    @Provides
    fun provideAppService(
        @NetworkModule.RetrofitBuilder retrofit: Retrofit
    ): AppService {
        return retrofit.create(AppService::class.java)
    }

    @Provides
    fun providePostService(
        @NetworkModule.RetrofitBuilder retrofit: Retrofit
    ): PostService {
        return retrofit.create(PostService::class.java)
    }
}