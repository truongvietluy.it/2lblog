package com.ausdilap.utils

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.ausdilap.utils.CameraUtils.cameraResult
import com.esafirm.imagepicker.features.ImagePicker
import timber.log.Timber

object CameraUtils {
    const val TAKE_PHOTO_TYPE = 1
    const val CHOOSE_SINGLE_PHOTO_TYPE = 2
    const val CHOOSE_MULTI_PHOTO_TYPE = 3

    const val RECORD_VIDEO = 4
    const val CHOOSE_VIDEO = 5

    val cameraResult: CameraResult = CameraResult()

    fun chooseSinglePhoto(context: Context?, callback: (imagePath: String) -> Unit) {
        cameraResult.resultCallBack = object : CameraResult.ResultCallBack {
            override fun onCompleted(data: Intent) {
                callback.invoke(ImagePicker.getFirstImageOrNull(data).path)
            }
        }
        CameraActivity.startActivity(context, CHOOSE_SINGLE_PHOTO_TYPE, CHOOSE_SINGLE_PHOTO_TYPE)
    }

    fun chooseMultiPhoto(context: Context?, limit: Int? = null, callback: (imagePaths: List<String>) -> Unit) {
        cameraResult.resultCallBack = object : CameraResult.ResultCallBack {
            override fun onCompleted(data: Intent) {
                callback.invoke(ImagePicker.getImages(data).map { it.path })
            }
        }

        CameraActivity.startActivity(context, CHOOSE_MULTI_PHOTO_TYPE, CHOOSE_MULTI_PHOTO_TYPE, limit)
    }

    fun takePhoto(context: Context?, callback: (imagePath: String) -> Unit) {
        cameraResult.resultCallBack = object : CameraResult.ResultCallBack {
            override fun onCompleted(data: Intent) {
                callback.invoke(ImagePicker.getFirstImageOrNull(data).path)
            }
        }
        CameraActivity.startActivity(context, TAKE_PHOTO_TYPE, TAKE_PHOTO_TYPE)
    }
}

class CameraResult {
    var resultCallBack: ResultCallBack? = null

    interface ResultCallBack {
        fun onCompleted(data: Intent)
    }
}

class CameraActivity : AppCompatActivity() {
    var requestCode: Int = 0

    companion object {
        const val REQUEST_CODE = "request_code"
        const val PHOTO_TYPE = "photo_type"
        const val LIMIT_IMAGE = "limit_image"

        fun startActivity(context: Context?, photoType: Int, requestCode: Int, limitImage: Int? = null) {
            context?.startActivity(Intent(context, CameraActivity::class.java).apply {
                putExtra(REQUEST_CODE, requestCode)
                putExtra(PHOTO_TYPE, photoType)
                putExtra(LIMIT_IMAGE, limitImage)
            })
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

        requestCode = intent.getIntExtra(REQUEST_CODE, 0)

        when (intent.getIntExtra(PHOTO_TYPE, CameraUtils.TAKE_PHOTO_TYPE)) {
            CameraUtils.TAKE_PHOTO_TYPE -> {
                ImagePicker
                    .cameraOnly()
                    .start(this, requestCode)
            }
            CameraUtils.CHOOSE_SINGLE_PHOTO_TYPE -> {
                ImagePicker
                    .create(this)
                    .showCamera(false)
                    .single()
                    .start(requestCode)
            }
            CameraUtils.CHOOSE_MULTI_PHOTO_TYPE -> {
                val limit = intent.getIntExtra(LIMIT_IMAGE, -1)
                if (limit != -1) {
                    ImagePicker
                        .create(this)
                        .showCamera(false)
                        .multi()
                        .limit(limit)
                        .start(requestCode)
                } else {
                    ImagePicker
                        .create(this)
                        .showCamera(false)
                        .multi()
                        .start(requestCode)
                }
            }
            else -> {
                finish()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Timber.d("/// onActivityResult: requestCode=$requestCode, resultCode=$resultCode, data=$data")
        if (resultCode == RESULT_OK && data != null) {
            when (requestCode) {
                CameraUtils.TAKE_PHOTO_TYPE,
                CameraUtils.CHOOSE_SINGLE_PHOTO_TYPE,
                CameraUtils.CHOOSE_MULTI_PHOTO_TYPE,
                CameraUtils.RECORD_VIDEO,
                CameraUtils.CHOOSE_VIDEO
                -> {
                    cameraResult.resultCallBack?.onCompleted(data)
                }
            }
        }
        finish()
    }
}