package com.ausdilap.utils

import android.content.Context
import android.net.Uri
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import timber.log.Timber
import java.io.File
import java.io.IOException

object FileHelper {
    /**
     * read file.txt Internal storage
     */
    fun readFromFile(context: Context, fileName: String): String? {
        return try {
            val file = File(context.filesDir, fileName)
            file.readText()
        } catch (e: IOException) {
            Timber.e("/// Read file failed: $e")
            null
        }
    }

    fun writeToFile(context: Context, fileName: String, value: String) {
        try {
            context.openFileOutput(fileName, Context.MODE_PRIVATE).use {
                it.write(value.toByteArray())
            }
        } catch (e: IOException) {
            Timber.e("/// Write file failed: $e")
        }
    }

    //    /**
    //     * read file.txt ExternalStorage
    //     */
    //    @Suppress("DEPRECATION")
    //    fun readFromFile(fileName: String): String? {
    //        return try {
    //            val root = Environment.getExternalStorageDirectory().toString()
    //            val dir = File("$root/fashion")
    //            if (!dir.exists()) dir.mkdirs()
    //            val file = File(dir, fileName)
    //            if (!file.exists()) file.createNewFile()
    //            file.readText()
    //        } catch (e: IOException) {
    //            Timber.e("Read file failed: $e")
    //            null
    //        }
    //    }
    //
    //    @Suppress("DEPRECATION")
    //    fun writeToFile(fileName: String, value: String) {
    //        try {
    //            val root = Environment.getExternalStorageDirectory().toString()
    //            val dir = File("$root/fashion")
    //            if (!dir.exists()) dir.mkdirs()
    //            val file = File(dir, fileName)
    //            if (!file.exists()) file.createNewFile()
    //            file.writeText(value)
    //        } catch (e: IOException) {
    //            Timber.e("Write file failed: $e")
    //        }
    //    }

    fun prepareFilePart(partName: String, fileUri: Uri): MultipartBody.Part {
        val file = File(fileUri.path.orEmpty())
        // create RequestBody instance from file
        val requestFile = file.asRequestBody(("image/*").toMediaTypeOrNull())
        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, file.name, requestFile)
    }
}