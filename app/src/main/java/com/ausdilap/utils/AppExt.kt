package com.ausdilap.utils

import android.content.Context
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.ausdilap.base.BaseViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers

/**
 * Created by pvduc9773 on 02/11/2021.
 */
fun Context.showToast(@StringRes message: Int) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Context.showToast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun <T> LifecycleOwner.observe(dataObserve: LiveData<T>, action: (t: T) -> Unit) {
    dataObserve.observe(this, Observer { it?.let { t -> action(t) } })
}

fun <T> LifecycleOwner.singleObserve(liveData: LiveData<T>, action: (t: T) -> Unit) {
    if (!liveData.hasObservers()) {
        liveData.observe(this) { it?.let { t -> action(t) } }
    }
}

fun <T> Observable<T>.applyBackgroundStream(): Observable<T> {
    return subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}

fun Disposable.addTo(vm: BaseViewModel) {
    vm.disposable.add(this)
}
